<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.2.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="14" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="yes" active="yes"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="top_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="134" name="silk_top" color="7" fill="1" visible="no" active="no"/>
<layer number="135" name="silk_bottom" color="7" fill="1" visible="no" active="no"/>
<layer number="136" name="silktop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="137" name="silkbottom" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="145" name="DrillLegend_01-16" color="7" fill="1" visible="yes" active="yes"/>
<layer number="146" name="DrillLegend_01-20" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="166" name="AntennaArea" color="7" fill="1" visible="yes" active="yes"/>
<layer number="168" name="4mmHeightArea" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="Accent" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="supply2">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Seeed-OPL-2017-01-Diode-james">
<packages>
<package name="LED4-SMD-5050">
<smd name="1" x="-2.45" y="1.65" dx="0.9" dy="1.5" layer="1" rot="R90"/>
<smd name="2" x="-2.45" y="-1.65" dx="0.9" dy="1.5" layer="1" rot="R90"/>
<smd name="3" x="2.45" y="-1.65" dx="0.9" dy="1.5" layer="1" rot="R90"/>
<smd name="4" x="2.45" y="1.65" dx="0.9" dy="1.5" layer="1" rot="R90"/>
<wire x1="-2.5" y1="2.5" x2="2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="2.5" y1="2.5" x2="2.5" y2="-1.4632625" width="0.127" layer="21"/>
<wire x1="2.5" y1="-1.4632625" x2="1.4632625" y2="-2.5" width="0.127" layer="21"/>
<wire x1="1.4632625" y1="-2.5" x2="-2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-2.5" x2="-2.5" y2="2.5" width="0.127" layer="21"/>
<rectangle x1="-2.413" y1="-2.54" x2="2.413" y2="2.54" layer="39"/>
<text x="-1.905" y="2.667" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-1.905" y="0" size="0.635" layer="27" ratio="11">&gt;VALUE</text>
<circle x="2.969" y="-2.861" radius="0.381" width="0" layer="21"/>
</package>
<package name="DO-214AA">
<wire x1="2.16" y1="-1.78" x2="-2.14" y2="-1.78" width="0.2032" layer="21"/>
<wire x1="-2.14" y1="-1.78" x2="-2.16" y2="-1.78" width="0.2032" layer="21"/>
<wire x1="-2.16" y1="-1.78" x2="-2.16" y2="1.78" width="0.2032" layer="51"/>
<wire x1="-2.16" y1="1.78" x2="-2.14" y2="1.78" width="0.2032" layer="21"/>
<wire x1="-2.14" y1="1.78" x2="2.16" y2="1.78" width="0.2032" layer="21"/>
<wire x1="2.16" y1="1.78" x2="2.16" y2="-1.78" width="0.2032" layer="51"/>
<wire x1="0.508" y1="0" x2="0.381" y2="-0.127" width="0.127" layer="21"/>
<wire x1="0.381" y1="-0.127" x2="0.254" y2="-0.254" width="0.127" layer="21"/>
<wire x1="0.254" y1="-0.254" x2="0.127" y2="-0.381" width="0.127" layer="21"/>
<wire x1="-0.381" y1="-0.889" x2="-0.508" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-0.508" y1="-1.016" x2="-0.508" y2="1.016" width="0.127" layer="21"/>
<wire x1="-0.508" y1="1.016" x2="-0.381" y2="0.889" width="0.127" layer="21"/>
<wire x1="-0.381" y1="0.889" x2="-0.254" y2="0.762" width="0.127" layer="21"/>
<wire x1="-0.381" y1="0.889" x2="-0.381" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-0.381" y1="-0.889" x2="-0.254" y2="-0.762" width="0.127" layer="21"/>
<wire x1="-0.254" y1="-0.762" x2="-0.127" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-0.254" y1="-0.762" x2="-0.254" y2="0.762" width="0.127" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.127" y2="0.635" width="0.127" layer="21"/>
<wire x1="-0.127" y1="0.635" x2="0" y2="0.508" width="0.127" layer="21"/>
<wire x1="-0.127" y1="0.635" x2="-0.127" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-0.127" y1="-0.635" x2="0" y2="-0.508" width="0.127" layer="21"/>
<wire x1="0" y1="-0.508" x2="0.127" y2="-0.381" width="0.127" layer="21"/>
<wire x1="0" y1="-0.508" x2="0" y2="0.508" width="0.127" layer="21"/>
<wire x1="0" y1="0.508" x2="0.127" y2="0.381" width="0.127" layer="21"/>
<wire x1="0.127" y1="0.381" x2="0.254" y2="0.254" width="0.127" layer="21"/>
<wire x1="0.127" y1="0.381" x2="0.127" y2="-0.381" width="0.127" layer="21"/>
<wire x1="0.254" y1="-0.254" x2="0.254" y2="0.254" width="0.127" layer="21"/>
<wire x1="0.254" y1="0.254" x2="0.381" y2="0.127" width="0.127" layer="21"/>
<wire x1="0.381" y1="0.127" x2="0.508" y2="0" width="0.127" layer="21"/>
<wire x1="0.381" y1="0.127" x2="0.381" y2="-0.127" width="0.127" layer="21"/>
<rectangle x1="-3.429" y1="-1.905" x2="3.429" y2="1.905" layer="39" rot="R180"/>
<smd name="+" x="-2.21" y="0" dx="2.743" dy="2.159" layer="1" rot="R270"/>
<smd name="-" x="2.21" y="0" dx="2.743" dy="2.159" layer="1" rot="R270"/>
<text x="-2.159" y="2.032" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-1.651" y="-2.794" size="0.635" layer="27" ratio="10">&gt;VALUE</text>
<text x="1.27" y="0" size="0.5" layer="33" ratio="10" rot="R180">&gt;NAME</text>
<wire x1="2.16" y1="-1.78" x2="2.16" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="2.16" y1="1.52" x2="2.16" y2="1.78" width="0.2032" layer="21"/>
<wire x1="-2.14" y1="1.52" x2="-2.14" y2="1.78" width="0.2032" layer="21"/>
<wire x1="-2.14" y1="-1.78" x2="-2.14" y2="-1.5" width="0.2032" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="LED-RGB-4P">
<pin name="VDD" x="-11.43" y="2.54" length="short"/>
<pin name="DOUT" x="-11.43" y="-1.27" length="short"/>
<pin name="VSS" x="11.43" y="-1.27" length="short" rot="R180"/>
<pin name="DIN" x="11.43" y="2.54" length="short" rot="R180"/>
<wire x1="-7.62" y1="6.35" x2="7.62" y2="6.35" width="0.1524" layer="94"/>
<wire x1="7.62" y1="6.35" x2="7.62" y2="2.54" width="0.1524" layer="94"/>
<wire x1="7.62" y1="2.54" x2="7.62" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-1.27" x2="7.62" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="-7.62" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-1.27" x2="-7.62" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="2.54" x2="-7.62" y2="6.35" width="0.1524" layer="94"/>
<wire x1="-8.89" y1="2.54" x2="-7.62" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-8.89" y1="-1.27" x2="-7.62" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="7.62" y1="2.54" x2="8.89" y2="2.54" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-1.27" x2="8.89" y2="-1.27" width="0.1524" layer="94"/>
<text x="-7.62" y="6.35" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="0" y="6.35" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
</symbol>
<symbol name="DIODE*-1">
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<text x="-3.81" y="2.54" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-3.81" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="+" x="-3.81" y="0" visible="off" length="short" direction="pas"/>
<pin name="-" x="3.81" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SMD-RGB-LED-4P" prefix="D" uservalue="yes">
<description>304990003</description>
<gates>
<gate name="G$1" symbol="LED-RGB-4P" x="0" y="0"/>
</gates>
<devices>
<device name="'WS2812B'" package="LED4-SMD-5050">
<connects>
<connect gate="G$1" pin="DIN" pad="4"/>
<connect gate="G$1" pin="DOUT" pad="2"/>
<connect gate="G$1" pin="VDD" pad="1"/>
<connect gate="G$1" pin="VSS" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SMD-DIODE-SCHOTTKY-28V-5A(DO-214AA)" prefix="D" uservalue="yes">
<description>304020021</description>
<gates>
<gate name="G$1" symbol="DIODE*-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DO-214AA">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="SS54B" constant="no"/>
<attribute name="VALUE" value="28V-5A"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Seeed-OPL-2017-01-Fuse-Akita">
<packages>
<package name="F1210">
<description>SMD, 1210(inch)</description>
<wire x1="-0.75" y1="1" x2="0.75" y2="1" width="0.127" layer="21"/>
<text x="-2" y="1.5" size="1.27" layer="25">&gt;NAME</text>
<text x="-2" y="-3" size="1.27" layer="27">&gt;VALUE</text>
<smd name="P$1" x="-1.5" y="0" dx="1" dy="1.9" layer="1"/>
<smd name="P$2" x="1.5" y="0" dx="1" dy="1.9" layer="1"/>
<wire x1="-0.75" y1="-1" x2="0.75" y2="-1" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="FUSE">
<description>general Fuse</description>
<pin name="P$1" x="-7.62" y="0" visible="off" length="middle" direction="pas"/>
<pin name="P$2" x="7.62" y="0" visible="off" length="middle" direction="pas" rot="R180"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94" curve="-180"/>
<text x="-5.08" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="307010043_SMD_PPTC_2A_1210" prefix="F">
<description>SMD PPTC 2A-6V;1210, DaRong;SMD1210R200SF, SKU: 307010043</description>
<gates>
<gate name="G$1" symbol="FUSE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="F1210">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Seeed-Connector -2016">
<packages>
<package name="MICRO-USB5+6P-SMD-0.65-B">
<wire x1="-3.7" y1="-5" x2="-3.7" y2="0" width="0.127" layer="51"/>
<wire x1="-3.7" y1="0" x2="3.7" y2="0" width="0.127" layer="51"/>
<wire x1="3.7" y1="0" x2="3.7" y2="-5" width="0.127" layer="51"/>
<wire x1="-3.7" y1="-5" x2="-4" y2="-5.6" width="0.127" layer="21"/>
<wire x1="3.7" y1="-5" x2="4" y2="-5.6" width="0.127" layer="21"/>
<wire x1="-3.7" y1="-5.1" x2="3.7" y2="-5.1" width="0.127" layer="21"/>
<wire x1="-3.3" y1="-5.5" x2="3.2" y2="-5.5" width="0.127" layer="21"/>
<wire x1="-3.3" y1="-5.5" x2="-4" y2="-5.6" width="0.127" layer="21"/>
<wire x1="3.2" y1="-5.5" x2="4" y2="-5.6" width="0.127" layer="21"/>
<wire x1="-3.175" y1="-5.715" x2="-3.175" y2="-4.318" width="0.127" layer="46"/>
<wire x1="-3.175" y1="-4.318" x2="-3.429" y2="-4.699" width="0.127" layer="46"/>
<wire x1="-3.175" y1="-4.318" x2="-2.921" y2="-4.699" width="0.127" layer="46"/>
<wire x1="3.175" y1="-5.715" x2="3.175" y2="-4.318" width="0.127" layer="46"/>
<wire x1="3.175" y1="-4.318" x2="2.921" y2="-4.699" width="0.127" layer="46"/>
<wire x1="3.175" y1="-4.318" x2="3.429" y2="-4.699" width="0.127" layer="46"/>
<pad name="S2" x="-3.3" y="-2.675" drill="0.508" diameter="1.016" shape="long" rot="R90"/>
<pad name="S5" x="3.3" y="-2.675" drill="0.508" diameter="1.016" shape="long" rot="R90"/>
<smd name="3" x="0" y="-0.25" dx="0.4" dy="1.5" layer="1" roundness="50" rot="R180"/>
<smd name="2" x="-0.65" y="-0.25" dx="0.4" dy="1.5" layer="1" roundness="50" rot="R180"/>
<smd name="4" x="0.65" y="-0.25" dx="0.4" dy="1.5" layer="1" roundness="50" rot="R180"/>
<smd name="5" x="1.3" y="-0.25" dx="0.4" dy="1.5" layer="1" roundness="50" rot="R180"/>
<smd name="1" x="-1.3" y="-0.25" dx="0.4" dy="1.5" layer="1" roundness="50" rot="R180"/>
<smd name="S1" x="-3.4" y="-0.427" dx="1.4" dy="1.6" layer="1" roundness="50" rot="R90"/>
<smd name="S6" x="3.4" y="-0.427" dx="1.4" dy="1.6" layer="1" roundness="50" rot="R90"/>
<smd name="S3" x="-1.2" y="-2.675" dx="1.9" dy="1.9" layer="1" roundness="50"/>
<smd name="S4" x="1.2" y="-2.675" dx="1.9" dy="1.9" layer="1" roundness="50"/>
<text x="-1.905" y="1.27" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-5.08" y="-4.445" size="0.889" layer="27" ratio="11" rot="R90">&gt;value</text>
<rectangle x1="-1.5" y1="-1.6" x2="1.5" y2="-1.2" layer="41"/>
<wire x1="-3.3" y1="-2.413" x2="-3.3" y2="-3.175" width="0.508" layer="46"/>
<wire x1="3.3" y1="-2.413" x2="3.3" y2="-3.175" width="0.508" layer="46"/>
<text x="-3.81" y="-6.35" size="0.4064" layer="46">2 slot drills with copper</text>
<wire x1="-1.6" y1="0" x2="-2.5" y2="0" width="0.127" layer="21"/>
<wire x1="1.6" y1="0" x2="2.4" y2="0" width="0.127" layer="21"/>
<wire x1="3.7" y1="-1.3" x2="3.7" y2="-1.7" width="0.127" layer="21"/>
<wire x1="3.7" y1="-3.8" x2="3.7" y2="-5" width="0.127" layer="21"/>
<wire x1="-3.7" y1="-5" x2="-3.7" y2="-3.8" width="0.127" layer="21"/>
<wire x1="-3.7" y1="-1.6" x2="-3.7" y2="-1.3" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="MICRO-USB5+4P">
<wire x1="8.89" y1="7.62" x2="8.89" y2="-7.62" width="0.254" layer="94"/>
<wire x1="8.89" y1="-7.62" x2="-8.89" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-8.89" y1="-7.62" x2="-8.89" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-8.89" y1="-3.81" x2="-8.89" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-8.89" y1="-1.27" x2="-8.89" y2="1.27" width="0.254" layer="94"/>
<wire x1="-8.89" y1="1.27" x2="-8.89" y2="3.81" width="0.254" layer="94"/>
<wire x1="-8.89" y1="3.81" x2="-8.89" y2="7.62" width="0.254" layer="94"/>
<wire x1="-8.89" y1="7.62" x2="8.89" y2="7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="3.81" x2="-8.89" y2="3.81" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="1.27" x2="-8.89" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="-1.27" x2="-8.89" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="-3.81" x2="-8.89" y2="-3.81" width="0.1524" layer="94"/>
<text x="-8.89" y="7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="1.27" y="7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND@5" x="13.97" y="-5.08" length="middle" rot="R180"/>
<pin name="ID" x="13.97" y="-2.54" length="middle" rot="R180"/>
<pin name="DP" x="13.97" y="0" length="middle" rot="R180"/>
<pin name="DM" x="13.97" y="2.54" length="middle" rot="R180"/>
<pin name="VCC" x="13.97" y="5.08" length="middle" rot="R180"/>
<pin name="GND@1" x="-12.7" y="3.81" length="short"/>
<pin name="GND@2" x="-12.7" y="1.27" length="short"/>
<pin name="GND@3" x="-12.7" y="-1.27" length="short"/>
<pin name="GND@4" x="-12.7" y="-3.81" length="short"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MICRO-USB-SMD-B-(10118193-0001LF)" prefix="USB" uservalue="yes">
<description>320010003</description>
<gates>
<gate name="G$1" symbol="MICRO-USB5+4P" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MICRO-USB5+6P-SMD-0.65-B">
<connects>
<connect gate="G$1" pin="DM" pad="2"/>
<connect gate="G$1" pin="DP" pad="3"/>
<connect gate="G$1" pin="GND@1" pad="S1"/>
<connect gate="G$1" pin="GND@2" pad="S2"/>
<connect gate="G$1" pin="GND@3" pad="S3 S4"/>
<connect gate="G$1" pin="GND@4" pad="S5 S6"/>
<connect gate="G$1" pin="GND@5" pad="5"/>
<connect gate="G$1" pin="ID" pad="4"/>
<connect gate="G$1" pin="VCC" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="10118193-0001LF" constant="no"/>
<attribute name="VALUE" value="10118193-0001LF" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Seeed-IC-2016">
<packages>
<package name="QFN32G-0.5-5X5MM">
<smd name="1" x="-2.45" y="1.75" dx="0.7" dy="0.25" layer="1" roundness="50"/>
<smd name="2" x="-2.45" y="1.25" dx="0.7" dy="0.25" layer="1" roundness="50"/>
<smd name="3" x="-2.45" y="0.75" dx="0.7" dy="0.25" layer="1" roundness="50"/>
<smd name="4" x="-2.45" y="0.25" dx="0.7" dy="0.25" layer="1" roundness="50"/>
<smd name="5" x="-2.45" y="-0.25" dx="0.7" dy="0.25" layer="1" roundness="50"/>
<smd name="6" x="-2.45" y="-0.75" dx="0.7" dy="0.25" layer="1" roundness="50"/>
<smd name="7" x="-2.45" y="-1.25" dx="0.7" dy="0.25" layer="1" roundness="50"/>
<smd name="8" x="-2.45" y="-1.75" dx="0.7" dy="0.25" layer="1" roundness="50"/>
<smd name="9" x="-1.75" y="-2.45" dx="0.7" dy="0.25" layer="1" roundness="50" rot="R90"/>
<smd name="10" x="-1.25" y="-2.45" dx="0.7" dy="0.25" layer="1" roundness="50" rot="R90"/>
<smd name="11" x="-0.75" y="-2.45" dx="0.7" dy="0.25" layer="1" roundness="50" rot="R90"/>
<smd name="12" x="-0.25" y="-2.45" dx="0.7" dy="0.25" layer="1" roundness="50" rot="R90"/>
<smd name="13" x="0.25" y="-2.45" dx="0.7" dy="0.25" layer="1" roundness="50" rot="R90"/>
<smd name="14" x="0.75" y="-2.45" dx="0.7" dy="0.25" layer="1" roundness="50" rot="R90"/>
<smd name="15" x="1.25" y="-2.45" dx="0.7" dy="0.25" layer="1" roundness="50" rot="R90"/>
<smd name="16" x="1.75" y="-2.45" dx="0.7" dy="0.25" layer="1" roundness="50" rot="R90"/>
<smd name="17" x="2.45" y="-1.75" dx="0.7" dy="0.25" layer="1" roundness="50" rot="R180"/>
<smd name="18" x="2.45" y="-1.25" dx="0.7" dy="0.25" layer="1" roundness="50" rot="R180"/>
<smd name="19" x="2.45" y="-0.75" dx="0.7" dy="0.25" layer="1" roundness="50" rot="R180"/>
<smd name="20" x="2.45" y="-0.25" dx="0.7" dy="0.25" layer="1" roundness="50" rot="R180"/>
<smd name="21" x="2.45" y="0.25" dx="0.7" dy="0.25" layer="1" roundness="50" rot="R180"/>
<smd name="22" x="2.45" y="0.75" dx="0.7" dy="0.25" layer="1" roundness="50" rot="R180"/>
<smd name="23" x="2.45" y="1.25" dx="0.7" dy="0.25" layer="1" roundness="50" rot="R180"/>
<smd name="24" x="2.45" y="1.75" dx="0.7" dy="0.25" layer="1" roundness="50" rot="R180"/>
<smd name="25" x="1.75" y="2.45" dx="0.7" dy="0.25" layer="1" roundness="50" rot="R270"/>
<smd name="26" x="1.25" y="2.45" dx="0.7" dy="0.25" layer="1" roundness="50" rot="R270"/>
<smd name="27" x="0.75" y="2.45" dx="0.7" dy="0.25" layer="1" roundness="50" rot="R270"/>
<smd name="28" x="0.25" y="2.45" dx="0.7" dy="0.25" layer="1" roundness="50" rot="R270"/>
<smd name="29" x="-0.25" y="2.45" dx="0.7" dy="0.25" layer="1" roundness="50" rot="R270"/>
<smd name="30" x="-0.75" y="2.45" dx="0.7" dy="0.25" layer="1" roundness="50" rot="R270"/>
<smd name="31" x="-1.25" y="2.45" dx="0.7" dy="0.25" layer="1" roundness="50" rot="R270"/>
<smd name="32" x="-1.75" y="2.45" dx="0.7" dy="0.25" layer="1" roundness="50" rot="R270"/>
<wire x1="-2.5" y1="-2.5" x2="-2.5" y2="1.992" width="0.127" layer="21"/>
<wire x1="-2.5" y1="1.992" x2="-1.992" y2="2.5" width="0.127" layer="21"/>
<wire x1="-1.992" y1="2.5" x2="2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="2.5" y1="2.5" x2="2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="2.5" y1="-2.5" x2="-2.5" y2="-2.5" width="0.127" layer="21"/>
<rectangle x1="-2.54" y1="-2.54" x2="2.54" y2="2.54" layer="39"/>
<circle x="-3.175" y="1.778" radius="0.254" width="0" layer="21"/>
<text x="-1.905" y="2.921" size="0.889" layer="21" ratio="11">&gt;NAME</text>
<text x="-1.778" y="0" size="0.635" layer="27" ratio="10">&gt;VALUE</text>
<smd name="33" x="0" y="0" dx="3.6" dy="3.6" layer="1" roundness="20"/>
</package>
<package name="SOT-223">
<description>&lt;b&gt;SMALL OUTLINE TRANSISTOR&lt;/b&gt;</description>
<wire x1="3.277" y1="1.778" x2="3.277" y2="-1.778" width="0.2032" layer="21"/>
<wire x1="3.277" y1="-1.778" x2="-3.277" y2="-1.778" width="0.2032" layer="21"/>
<wire x1="-3.277" y1="-1.778" x2="-3.277" y2="1.778" width="0.2032" layer="21"/>
<wire x1="-3.277" y1="1.778" x2="3.277" y2="1.778" width="0.2032" layer="21"/>
<wire x1="-3.473" y1="4.483" x2="3.473" y2="4.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-4.483" x2="-3.473" y2="-4.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-4.483" x2="-3.473" y2="4.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="4.483" x2="3.473" y2="-4.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.286" y="-3.175" dx="1.27" dy="2.286" layer="1"/>
<smd name="2" x="0" y="-3.175" dx="1.27" dy="2.286" layer="1"/>
<smd name="3" x="2.286" y="-3.175" dx="1.27" dy="2.286" layer="1"/>
<smd name="TER" x="0" y="3.175" dx="3.556" dy="2.159" layer="1"/>
<text x="-3.81" y="-3.81" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="4.445" y="-3.81" size="0.889" layer="27" ratio="11" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.6002" y1="1.8034" x2="1.6002" y2="3.6576" layer="51"/>
<rectangle x1="-0.4318" y1="-3.6576" x2="0.4318" y2="-1.8034" layer="51"/>
<rectangle x1="-2.7432" y1="-3.6576" x2="-1.8796" y2="-1.8034" layer="51"/>
<rectangle x1="1.8796" y1="-3.6576" x2="2.7432" y2="-1.8034" layer="51"/>
<rectangle x1="-1.6002" y1="1.8034" x2="1.6002" y2="3.6576" layer="51"/>
<rectangle x1="-0.4318" y1="-3.6576" x2="0.4318" y2="-1.8034" layer="51"/>
<rectangle x1="-2.7432" y1="-3.6576" x2="-1.8796" y2="-1.8034" layer="51"/>
<rectangle x1="1.8796" y1="-3.6576" x2="2.7432" y2="-1.8034" layer="51"/>
<rectangle x1="-1" y1="-1" x2="1" y2="1" layer="35"/>
</package>
</packages>
<symbols>
<symbol name="MCU-ATMEGA328P-MU">
<wire x1="-17.78" y1="27.94" x2="20.32" y2="27.94" width="0.254" layer="94"/>
<wire x1="20.32" y1="27.94" x2="20.32" y2="-43.18" width="0.254" layer="94"/>
<wire x1="20.32" y1="-43.18" x2="-17.78" y2="-43.18" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-43.18" x2="-17.78" y2="27.94" width="0.254" layer="94"/>
<text x="-5.334" y="28.702" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-16.51" y="-45.72" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="PB5(SCK/PCINT5)" x="25.4" y="12.7" length="middle" rot="R180"/>
<pin name="PB7(XTAL2/TOSC2/PCINT7)" x="25.4" y="2.54" length="middle" rot="R180"/>
<pin name="PB6(XTAL1/TOSC1/PCINT6)" x="25.4" y="7.62" length="middle" rot="R180"/>
<pin name="GND@1" x="-22.86" y="-22.86" length="middle"/>
<pin name="GND@2" x="-22.86" y="-20.32" length="middle"/>
<pin name="VCC@1" x="-22.86" y="-7.62" length="middle"/>
<pin name="VCC@2" x="-22.86" y="-10.16" length="middle"/>
<pin name="GND" x="-22.86" y="20.32" length="middle"/>
<pin name="AREF" x="-22.86" y="17.78" length="middle"/>
<pin name="AVCC" x="-22.86" y="15.24" length="middle"/>
<pin name="PB4(MISO/PCINT4)" x="25.4" y="15.24" length="middle" rot="R180"/>
<pin name="PB3(MOSI/OC2/PCINT3)" x="25.4" y="17.78" length="middle" rot="R180"/>
<pin name="PB2(SS/OC1B/PCINT2)" x="25.4" y="20.32" length="middle" rot="R180"/>
<pin name="PB1(OC1A/PCINT1)" x="25.4" y="22.86" length="middle" rot="R180"/>
<pin name="PB0(ICP/CLKO/PCINT0)" x="25.4" y="25.4" length="middle" rot="R180"/>
<pin name="PD7(AIN1/PCINT23)" x="25.4" y="-40.64" length="middle" rot="R180"/>
<pin name="PD6(AIN0/PC0A/PCINT22)" x="25.4" y="-38.1" length="middle" rot="R180"/>
<pin name="PD5(T1/OC0B/PCINT21)" x="25.4" y="-35.56" length="middle" rot="R180"/>
<pin name="PD4(XCK/T0/CINT20)" x="25.4" y="-33.02" length="middle" rot="R180"/>
<pin name="PD3(INT1/OC2B/PCINT19)" x="25.4" y="-30.48" length="middle" rot="R180"/>
<pin name="PD2(INT0/PCINT18)" x="25.4" y="-27.94" length="middle" rot="R180"/>
<pin name="PD1(TXD/PCINT17)" x="25.4" y="-25.4" length="middle" rot="R180"/>
<pin name="PD0(RXD/PCINT16)" x="25.4" y="-22.86" length="middle" rot="R180"/>
<pin name="ADC7" x="-22.86" y="0" length="middle"/>
<pin name="PC6(PCINT14/!RESET)" x="25.4" y="-17.78" length="middle" rot="R180"/>
<pin name="PC5(ADC5/SCL/PCINT13)" x="25.4" y="-15.24" length="middle" rot="R180"/>
<pin name="PC4(ADC4/SDA/PCINT12)" x="25.4" y="-12.7" length="middle" rot="R180"/>
<pin name="PC3(ADC3/PCINT11)" x="25.4" y="-10.16" length="middle" rot="R180"/>
<pin name="PC2(ADC2PCINT10)" x="25.4" y="-7.62" length="middle" rot="R180"/>
<pin name="PC1(ADC1PCINT9)" x="25.4" y="-5.08" length="middle" rot="R180"/>
<pin name="PC0(ADC0/PCINT8)" x="25.4" y="-2.54" length="middle" rot="R180"/>
<pin name="PAD" x="-22.86" y="-33.02" length="middle"/>
<pin name="ADC6" x="-22.86" y="5.08" length="middle"/>
</symbol>
<symbol name="PMIC-CJ*1117">
<text x="-7.62" y="3.81" size="1.27" layer="95">&gt;NAME</text>
<text x="0" y="3.81" size="1.27" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="-7.62" length="short" rot="R90"/>
<pin name="IN" x="-11.43" y="0" length="short"/>
<pin name="OUT" x="11.43" y="0" length="short" rot="R180"/>
<wire x1="-7.62" y1="3.81" x2="7.62" y2="3.81" width="0.1524" layer="94"/>
<wire x1="7.62" y1="3.81" x2="7.62" y2="0" width="0.1524" layer="94"/>
<wire x1="7.62" y1="0" x2="7.62" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-3.81" x2="0" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="0" y1="-3.81" x2="-7.62" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-3.81" x2="-7.62" y2="0" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="3.81" width="0.1524" layer="94"/>
<wire x1="-8.89" y1="0" x2="-7.62" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="-5.08" x2="0" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="7.62" y1="0" x2="8.89" y2="0" width="0.1524" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MCU-ATMEGA328P-MU(QFN32)" prefix="U" uservalue="yes">
<description>310010005</description>
<gates>
<gate name="G$1" symbol="MCU-ATMEGA328P-MU" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFN32G-0.5-5X5MM">
<connects>
<connect gate="G$1" pin="ADC6" pad="19"/>
<connect gate="G$1" pin="ADC7" pad="22"/>
<connect gate="G$1" pin="AREF" pad="20"/>
<connect gate="G$1" pin="AVCC" pad="18"/>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="GND@1" pad="5"/>
<connect gate="G$1" pin="GND@2" pad="21"/>
<connect gate="G$1" pin="PAD" pad="33"/>
<connect gate="G$1" pin="PB0(ICP/CLKO/PCINT0)" pad="12"/>
<connect gate="G$1" pin="PB1(OC1A/PCINT1)" pad="13"/>
<connect gate="G$1" pin="PB2(SS/OC1B/PCINT2)" pad="14"/>
<connect gate="G$1" pin="PB3(MOSI/OC2/PCINT3)" pad="15"/>
<connect gate="G$1" pin="PB4(MISO/PCINT4)" pad="16"/>
<connect gate="G$1" pin="PB5(SCK/PCINT5)" pad="17"/>
<connect gate="G$1" pin="PB6(XTAL1/TOSC1/PCINT6)" pad="7"/>
<connect gate="G$1" pin="PB7(XTAL2/TOSC2/PCINT7)" pad="8"/>
<connect gate="G$1" pin="PC0(ADC0/PCINT8)" pad="23"/>
<connect gate="G$1" pin="PC1(ADC1PCINT9)" pad="24"/>
<connect gate="G$1" pin="PC2(ADC2PCINT10)" pad="25"/>
<connect gate="G$1" pin="PC3(ADC3/PCINT11)" pad="26"/>
<connect gate="G$1" pin="PC4(ADC4/SDA/PCINT12)" pad="27"/>
<connect gate="G$1" pin="PC5(ADC5/SCL/PCINT13)" pad="28"/>
<connect gate="G$1" pin="PC6(PCINT14/!RESET)" pad="29"/>
<connect gate="G$1" pin="PD0(RXD/PCINT16)" pad="30"/>
<connect gate="G$1" pin="PD1(TXD/PCINT17)" pad="31"/>
<connect gate="G$1" pin="PD2(INT0/PCINT18)" pad="32"/>
<connect gate="G$1" pin="PD3(INT1/OC2B/PCINT19)" pad="1"/>
<connect gate="G$1" pin="PD4(XCK/T0/CINT20)" pad="2"/>
<connect gate="G$1" pin="PD5(T1/OC0B/PCINT21)" pad="9"/>
<connect gate="G$1" pin="PD6(AIN0/PC0A/PCINT22)" pad="10"/>
<connect gate="G$1" pin="PD7(AIN1/PCINT23)" pad="11"/>
<connect gate="G$1" pin="VCC@1" pad="4"/>
<connect gate="G$1" pin="VCC@2" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="ATMEGA328P-MU" constant="no"/>
<attribute name="VALUE" value="ATMEGA328P-MU-QFN32" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PMIC-CJT1117-3.3(SOT223)" prefix="U">
<description>310030097</description>
<gates>
<gate name="G$1" symbol="PMIC-CJ*1117" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT-223">
<connects>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="IN" pad="3"/>
<connect gate="G$1" pin="OUT" pad="2 TER"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="CJT1117-3.3"/>
<attribute name="VALUE" value="CJT1117-3.3-SOT223" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find connectors and sockets- basically anything that can be plugged into or onto.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="1X06">
<wire x1="5.08" y1="0.635" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.985" y2="1.27" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="7.62" y2="0.635" width="0.2032" layer="21"/>
<wire x1="7.62" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.985" y1="-1.27" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="5.08" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.635" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.08" y2="0.635" width="0.2032" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="2.54" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.2032" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-2.54" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="0" y2="0.635" width="0.2032" layer="21"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-6.985" y1="1.27" x2="-5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-5.715" y1="1.27" x2="-5.08" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-2.54" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-4.445" y1="-1.27" x2="-5.08" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-7.62" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-6.985" y1="1.27" x2="-7.62" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-7.62" y1="-0.635" x2="-6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-5.715" y1="-1.27" x2="-6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="7.62" y1="0.635" x2="7.62" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="-3.81" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="-1.27" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="1.27" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="3.81" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="6.35" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-7.6962" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-7.62" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-6.604" y1="-0.254" x2="-6.096" y2="0.254" layer="51"/>
</package>
<package name="MOLEX-1X6">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="13.97" y1="3.048" x2="13.97" y2="-2.54" width="0.127" layer="21"/>
<wire x1="13.97" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="13.97" y1="-2.54" x2="12.7" y2="-2.54" width="0.127" layer="21"/>
<wire x1="12.7" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="12.7" y2="-1.27" width="0.127" layer="21"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.8796"/>
</package>
<package name="MOLEX-1X6-RA">
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="3.175" width="0.127" layer="21"/>
<wire x1="13.97" y1="0.635" x2="13.97" y2="3.175" width="0.127" layer="21"/>
<wire x1="13.97" y1="0.635" x2="-1.27" y2="0.635" width="0.127" layer="21"/>
<wire x1="13.97" y1="3.175" x2="12.7" y2="3.175" width="0.127" layer="21"/>
<wire x1="12.7" y1="3.175" x2="0" y2="3.175" width="0.127" layer="21"/>
<wire x1="0" y1="3.175" x2="-1.27" y2="3.175" width="0.127" layer="21"/>
<wire x1="0" y1="3.175" x2="0" y2="7.62" width="0.127" layer="21"/>
<wire x1="0" y1="7.62" x2="12.7" y2="7.62" width="0.127" layer="21"/>
<wire x1="12.7" y1="7.62" x2="12.7" y2="3.175" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.8796"/>
<text x="-0.889" y="-2.794" size="1.27" layer="25">&gt;NAME</text>
<text x="8.001" y="-2.794" size="1.27" layer="25">&gt;VALUE</text>
</package>
<package name="1X06-SMD">
<wire x1="7.62" y1="1.25" x2="-7.62" y2="1.25" width="0.127" layer="51"/>
<wire x1="-7.62" y1="1.25" x2="-7.62" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-7.62" y1="-1.25" x2="-6.35" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-6.35" y1="-1.25" x2="-3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="6.35" y2="-1.25" width="0.127" layer="51"/>
<wire x1="6.35" y1="-1.25" x2="7.62" y2="-1.25" width="0.127" layer="51"/>
<wire x1="7.62" y1="-1.25" x2="7.62" y2="1.25" width="0.127" layer="51"/>
<wire x1="6.35" y1="-1.25" x2="6.35" y2="-7.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="-7.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="-1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-3.81" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-6.35" y1="-1.25" x2="-6.35" y2="-7.25" width="0.127" layer="51"/>
<smd name="4" x="1.27" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="5" x="3.81" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="6" x="6.35" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="3" x="-1.27" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-3.81" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="1" x="-6.35" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<hole x="-5.08" y="0" drill="1.4"/>
<hole x="5.08" y="0" drill="1.4"/>
<text x="-1.905" y="1.905" size="0.8128" layer="25">&gt;NAME</text>
<text x="-1.905" y="0.635" size="0.8128" layer="27">&gt;VALUE</text>
</package>
<package name="1X06_LOCK">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="-1.27" y1="0.508" x2="-0.635" y2="1.143" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.143" x2="0.635" y2="1.143" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.143" x2="1.27" y2="0.508" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.508" x2="1.905" y2="1.143" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.143" x2="3.175" y2="1.143" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.143" x2="3.81" y2="0.508" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.508" x2="4.445" y2="1.143" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.143" x2="5.715" y2="1.143" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.143" x2="6.35" y2="0.508" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.508" x2="6.985" y2="1.143" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.143" x2="8.255" y2="1.143" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.143" x2="8.89" y2="0.508" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.508" x2="9.525" y2="1.143" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.143" x2="10.795" y2="1.143" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.143" x2="11.43" y2="0.508" width="0.2032" layer="21"/>
<wire x1="11.43" y1="0.508" x2="12.065" y2="1.143" width="0.2032" layer="21"/>
<wire x1="12.065" y1="1.143" x2="13.335" y2="1.143" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.143" x2="13.97" y2="0.508" width="0.2032" layer="21"/>
<wire x1="13.97" y1="0.508" x2="13.97" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.762" x2="13.335" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.397" x2="12.065" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.397" x2="11.43" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.762" x2="10.795" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.397" x2="9.525" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.397" x2="8.89" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.762" x2="8.255" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.397" x2="6.985" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="6.985" y1="-1.397" x2="6.35" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.762" x2="5.715" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.397" x2="4.445" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.397" x2="3.81" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.762" x2="3.175" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.397" x2="1.905" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.397" x2="1.27" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.762" x2="0.635" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.397" x2="-0.635" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.397" x2="-1.27" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.762" x2="-1.27" y2="0.508" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.508" x2="1.27" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.508" x2="3.81" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.508" x2="6.35" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.508" x2="8.89" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="11.43" y1="0.508" x2="11.43" y2="-0.762" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" diameter="1.8796"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="12.7" y="-0.254" drill="1.016" diameter="1.8796"/>
<text x="-1.27" y="1.778" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
<rectangle x1="7.3279" y1="-0.4191" x2="7.9121" y2="0.1651" layer="51"/>
<rectangle x1="9.8679" y1="-0.4191" x2="10.4521" y2="0.1651" layer="51"/>
<rectangle x1="12.4079" y1="-0.4191" x2="12.9921" y2="0.1651" layer="51"/>
</package>
<package name="1X06_LOCK_LONGPADS">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="1.524" y1="-0.127" x2="1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="4.064" y1="-0.127" x2="3.556" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="6.604" y1="-0.127" x2="6.096" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="9.144" y1="-0.127" x2="8.636" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="11.684" y1="-0.127" x2="11.176" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.8636" x2="-0.9906" y2="1.143" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.1176" x2="-0.9906" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.127" x2="13.716" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.127" x2="13.97" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-1.1176" x2="13.6906" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.127" x2="13.97" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="13.97" y1="0.8636" x2="13.6906" y2="1.143" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="6" x="12.7" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<text x="-1.27" y="1.778" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
<rectangle x1="7.3279" y1="-0.4191" x2="7.9121" y2="0.1651" layer="51" rot="R90"/>
<rectangle x1="9.8679" y1="-0.4191" x2="10.4521" y2="0.1651" layer="51"/>
<rectangle x1="12.4079" y1="-0.4191" x2="12.9921" y2="0.1651" layer="51"/>
</package>
<package name="MOLEX-1X6_LOCK">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="13.97" y1="3.048" x2="13.97" y2="-2.54" width="0.127" layer="21"/>
<wire x1="13.97" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="13.97" y1="-2.54" x2="12.7" y2="-2.54" width="0.127" layer="21"/>
<wire x1="12.7" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="12.7" y2="-1.27" width="0.127" layer="21"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" diameter="1.8796"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="12.7" y="-0.254" drill="1.016" diameter="1.8796"/>
<text x="-1.27" y="3.302" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-4.064" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
<rectangle x1="7.3279" y1="-0.4191" x2="7.9121" y2="0.1651" layer="51"/>
<rectangle x1="9.8679" y1="-0.4191" x2="10.4521" y2="0.1651" layer="51"/>
<rectangle x1="12.4079" y1="-0.4191" x2="12.9921" y2="0.1651" layer="51"/>
</package>
<package name="MOLEX-1X6-RA_LOCK">
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="3.175" width="0.127" layer="21"/>
<wire x1="13.97" y1="0.635" x2="13.97" y2="3.175" width="0.127" layer="21"/>
<wire x1="13.97" y1="0.635" x2="-1.27" y2="0.635" width="0.127" layer="21"/>
<wire x1="13.97" y1="3.175" x2="12.7" y2="3.175" width="0.127" layer="21"/>
<wire x1="12.7" y1="3.175" x2="0" y2="3.175" width="0.127" layer="21"/>
<wire x1="0" y1="3.175" x2="-1.27" y2="3.175" width="0.127" layer="21"/>
<wire x1="0" y1="3.175" x2="0" y2="7.62" width="0.127" layer="21"/>
<wire x1="0" y1="7.62" x2="12.7" y2="7.62" width="0.127" layer="21"/>
<wire x1="12.7" y1="7.62" x2="12.7" y2="3.175" width="0.127" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="5" x="10.16" y="0.127" drill="1.016" diameter="1.8796"/>
<pad name="6" x="12.7" y="-0.127" drill="1.016" diameter="1.8796"/>
<text x="-0.889" y="-2.794" size="1.27" layer="25">&gt;NAME</text>
<text x="8.001" y="-2.794" size="1.27" layer="25">&gt;VALUE</text>
</package>
<package name="1X06-SIP_LOCK">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="11.43" y1="0.635" x2="12.065" y2="1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="1.27" x2="13.335" y2="1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="13.335" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.27" x2="12.065" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="9.525" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.795" y2="1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.27" x2="11.43" y2="0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.635" x2="10.795" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.97" y1="0.635" x2="13.97" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" diameter="1.8796"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="12.7" y="-0.254" drill="1.016" diameter="1.8796"/>
<text x="-1.27" y="1.524" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-2.921" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
<rectangle x1="7.3279" y1="-0.4191" x2="7.9121" y2="0.1651" layer="51"/>
<rectangle x1="9.8679" y1="-0.4191" x2="10.4521" y2="0.1651" layer="51"/>
<rectangle x1="12.4079" y1="-0.4191" x2="12.9921" y2="0.1651" layer="51"/>
</package>
<package name="1X06_FEMALE_LOCK.010">
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="13.97" y2="1.27" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.97" y1="1.27" x2="13.97" y2="-1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.254" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.254" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0.254" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="-0.254" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3175" y1="-0.1905" x2="0.3175" y2="0.1905" layer="51"/>
<rectangle x1="2.2225" y1="-0.1905" x2="2.8575" y2="0.1905" layer="51"/>
<rectangle x1="4.7625" y1="-0.1905" x2="5.3975" y2="0.1905" layer="51"/>
<rectangle x1="7.3025" y1="-0.1905" x2="7.9375" y2="0.1905" layer="51"/>
<rectangle x1="9.8425" y1="-0.1905" x2="10.4775" y2="0.1905" layer="51"/>
<rectangle x1="12.3825" y1="-0.1905" x2="13.0175" y2="0.1905" layer="51"/>
</package>
<package name="1X06_LONGPADS">
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="0.635" x2="13.97" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<text x="-1.3462" y="2.4638" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X06-SMD_EDGE">
<wire x1="13.97" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="-11.176" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-11.176" x2="13.97" y2="-11.176" width="0.127" layer="51"/>
<wire x1="13.97" y1="-11.176" x2="13.97" y2="-2.54" width="0.127" layer="51"/>
<smd name="4" x="7.62" y="-0.08" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="5" x="10.16" y="-0.08" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="6" x="12.7" y="-0.08" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="3" x="5.08" y="-0.08" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="2" x="2.54" y="-0.08" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="1" x="0" y="-0.08" dx="3" dy="1" layer="1" rot="R90"/>
<text x="0" y="-6.35" size="0.4064" layer="51">thru-hole vertical Female Header</text>
<text x="0" y="-7.62" size="0.4064" layer="51">used as an SMD part </text>
<text x="0" y="-8.89" size="0.4064" layer="51">(placed horizontally, at board's edge)</text>
<rectangle x1="-0.381" y1="-2.4892" x2="0.381" y2="0.2794" layer="51"/>
<rectangle x1="2.159" y1="-2.4892" x2="2.921" y2="0.2794" layer="51"/>
<rectangle x1="4.699" y1="-2.4892" x2="5.461" y2="0.2794" layer="51"/>
<rectangle x1="7.239" y1="-2.4892" x2="8.001" y2="0.2794" layer="51"/>
<rectangle x1="9.779" y1="-2.4892" x2="10.541" y2="0.2794" layer="51"/>
<rectangle x1="12.319" y1="-2.4892" x2="13.081" y2="0.2794" layer="51"/>
</package>
<package name="SCREWTERMINAL-3.5MM-6">
<wire x1="-1.75" y1="3.4" x2="19.25" y2="3.4" width="0.2032" layer="21"/>
<wire x1="19.25" y1="3.4" x2="19.25" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="19.25" y1="-2.8" x2="19.25" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="19.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="19.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.25" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-1.35" x2="-2.25" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="19.25" y1="3.15" x2="19.75" y2="3.15" width="0.2032" layer="51"/>
<wire x1="19.75" y1="3.15" x2="19.75" y2="2.15" width="0.2032" layer="51"/>
<wire x1="19.75" y1="2.15" x2="19.25" y2="2.15" width="0.2032" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="7" y="0" drill="1.2" diameter="2.032"/>
<pad name="4" x="10.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="5" x="14" y="0" drill="1.2" diameter="2.032"/>
<pad name="6" x="17.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X06-SMD-FEMALE">
<wire x1="-7.62" y1="4.05" x2="7.62" y2="4.05" width="0.2032" layer="51"/>
<wire x1="7.62" y1="4.05" x2="7.62" y2="-4.05" width="0.2032" layer="51"/>
<wire x1="7.62" y1="-4.05" x2="-7.62" y2="-4.05" width="0.2032" layer="51"/>
<wire x1="-7.62" y1="-4.05" x2="-7.62" y2="4.05" width="0.2032" layer="51"/>
<wire x1="-6.85" y1="-3" x2="-6.85" y2="-2" width="0.2032" layer="21"/>
<wire x1="-6.85" y1="-2" x2="-5.85" y2="-2" width="0.2032" layer="21"/>
<wire x1="-5.85" y1="-2" x2="-5.85" y2="-3" width="0.2032" layer="21"/>
<wire x1="-7.1" y1="4" x2="-7.6" y2="4" width="0.2032" layer="21"/>
<wire x1="-7.6" y1="4" x2="-7.6" y2="-4" width="0.2032" layer="21"/>
<wire x1="-7.6" y1="-4" x2="-7.1" y2="-4" width="0.2032" layer="21"/>
<wire x1="7.1" y1="4" x2="7.6" y2="4" width="0.2032" layer="21"/>
<wire x1="7.6" y1="4" x2="7.6" y2="-4" width="0.2032" layer="21"/>
<wire x1="7.6" y1="-4" x2="7.1" y2="-4" width="0.2032" layer="21"/>
<wire x1="-0.3" y1="4" x2="0.3" y2="4" width="0.2032" layer="21"/>
<wire x1="-2.8" y1="4" x2="-2.2" y2="4" width="0.2032" layer="21"/>
<wire x1="-5.4" y1="4" x2="-4.8" y2="4" width="0.2032" layer="21"/>
<wire x1="2.3" y1="4" x2="2.9" y2="4" width="0.2032" layer="21"/>
<wire x1="4.8" y1="4" x2="5.4" y2="4" width="0.2032" layer="21"/>
<wire x1="4.8" y1="-4" x2="5.4" y2="-4" width="0.2032" layer="21"/>
<wire x1="2.2" y1="-4" x2="2.8" y2="-4" width="0.2032" layer="21"/>
<wire x1="-0.3" y1="-4" x2="0.3" y2="-4" width="0.2032" layer="21"/>
<wire x1="-2.8" y1="-4" x2="-2.2" y2="-4" width="0.2032" layer="21"/>
<wire x1="-5.4" y1="-4" x2="-4.8" y2="-4" width="0.2032" layer="21"/>
<smd name="6" x="6.35" y="-4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="5" x="3.81" y="-4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="4" x="1.27" y="-4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="3" x="-1.27" y="-4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="2" x="-3.81" y="-4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="1" x="-6.35" y="-4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="A1" x="-6.35" y="4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="A2" x="-3.81" y="4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="A3" x="-1.27" y="4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="A4" x="1.27" y="4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="A5" x="3.81" y="4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="A6" x="6.35" y="4.62" dx="1.02" dy="2.16" layer="1"/>
</package>
<package name="1X06-SMD-FEMALE-V2">
<description>Package for 4UCONN part #19686&lt;br /&gt;
Used on FTDI breakouts</description>
<wire x1="-7.5" y1="0.45" x2="-7.5" y2="-8.05" width="0.127" layer="21"/>
<wire x1="7.5" y1="0.45" x2="-7.5" y2="0.45" width="0.127" layer="21"/>
<wire x1="7.5" y1="-8.05" x2="7.5" y2="0.45" width="0.127" layer="21"/>
<wire x1="-7.5" y1="-8.05" x2="7.5" y2="-8.05" width="0.127" layer="21"/>
<smd name="4" x="-1.27" y="3.425" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="5" x="-3.81" y="3.425" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="6" x="-6.35" y="3.425" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="3" x="1.27" y="3.425" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="2" x="3.81" y="3.425" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="1" x="6.35" y="3.425" dx="1.25" dy="3" layer="1" rot="R180"/>
<text x="7.6" y="-8.3" size="1" layer="27" rot="R180">&gt;Value</text>
<text x="-7.4" y="-9.3" size="1" layer="25">&gt;Name</text>
</package>
<package name="1X06_HOLES_ONLY">
<circle x="0" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="2.54" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="5.08" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="7.62" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="10.16" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="12.7" y="0" radius="0.635" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<hole x="0" y="0" drill="1.4732"/>
<hole x="2.54" y="0" drill="1.4732"/>
<hole x="5.08" y="0" drill="1.4732"/>
<hole x="7.62" y="0" drill="1.4732"/>
<hole x="10.16" y="0" drill="1.4732"/>
<hole x="12.7" y="0" drill="1.4732"/>
</package>
<package name="1X06_SMD_STRAIGHT">
<wire x1="1.37" y1="1.25" x2="-14.07" y2="1.25" width="0.127" layer="51"/>
<wire x1="-14.07" y1="1.25" x2="-14.07" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-14.07" y1="-1.25" x2="1.37" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.37" y1="-1.25" x2="1.37" y2="1.25" width="0.127" layer="51"/>
<wire x1="1.37" y1="1.25" x2="1.37" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-14.07" y1="-1.25" x2="-14.07" y2="1.25" width="0.127" layer="21"/>
<wire x1="0.85" y1="1.25" x2="1.37" y2="1.25" width="0.127" layer="21"/>
<wire x1="-14.07" y1="1.25" x2="-10.883" y2="1.25" width="0.127" layer="21"/>
<wire x1="-13.55" y1="-1.25" x2="-14.07" y2="-1.25" width="0.127" layer="21"/>
<wire x1="1.37" y1="-1.25" x2="-1.817" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-4.377" y1="1.25" x2="-0.703" y2="1.25" width="0.127" layer="21"/>
<wire x1="-9.457" y1="1.25" x2="-5.783" y2="1.25" width="0.127" layer="21"/>
<wire x1="-3.329" y1="-1.25" x2="-6.831" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-8.409" y1="-1.25" x2="-11.911" y2="-1.25" width="0.127" layer="21"/>
<smd name="5" x="-10.16" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="3" x="-5.08" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="1" x="0" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="6" x="-12.7" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="4" x="-7.62" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-2.54" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<text x="-13.97" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-13.97" y="-4.191" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="1X06_SMD_STRAIGHT_ALT">
<wire x1="1.37" y1="1.25" x2="-14.07" y2="1.25" width="0.127" layer="51"/>
<wire x1="-14.07" y1="1.25" x2="-14.07" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-14.07" y1="-1.25" x2="1.37" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.37" y1="-1.25" x2="1.37" y2="1.25" width="0.127" layer="51"/>
<wire x1="-14.07" y1="1.25" x2="-14.07" y2="-1.25" width="0.127" layer="21"/>
<wire x1="1.37" y1="-1.25" x2="1.37" y2="1.25" width="0.127" layer="21"/>
<wire x1="-13.55" y1="1.25" x2="-14.07" y2="1.25" width="0.127" layer="21"/>
<wire x1="1.37" y1="1.25" x2="-1.817" y2="1.25" width="0.127" layer="21"/>
<wire x1="0.85" y1="-1.25" x2="1.37" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-14.07" y1="-1.25" x2="-10.883" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-8.323" y1="1.25" x2="-11.997" y2="1.25" width="0.127" layer="21"/>
<wire x1="-3.243" y1="1.25" x2="-6.917" y2="1.25" width="0.127" layer="21"/>
<wire x1="-9.371" y1="-1.25" x2="-5.869" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-4.291" y1="-1.25" x2="-0.789" y2="-1.25" width="0.127" layer="21"/>
<smd name="5" x="-10.16" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="3" x="-5.08" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1" x="0" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="6" x="-12.7" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="4" x="-7.62" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="2" x="-2.54" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<text x="-13.97" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-13.97" y="-4.191" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="1X06_SMD_STRAIGHT_COMBO">
<wire x1="12.7" y1="1.27" x2="12.7" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="10.16" y1="1.27" x2="10.16" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="7.62" y1="1.27" x2="7.62" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="-1.37" y1="-1.25" x2="-1.37" y2="1.25" width="0.1778" layer="21"/>
<wire x1="14.07" y1="1.25" x2="14.07" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="-0.73" y1="-1.25" x2="-1.37" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="14.07" y1="-1.25" x2="13.4" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="13.4" y1="1.25" x2="14.07" y2="1.25" width="0.1778" layer="21"/>
<wire x1="-1.37" y1="1.25" x2="-0.73" y2="1.25" width="0.1778" layer="21"/>
<wire x1="10.949" y1="1.25" x2="11.911" y2="1.25" width="0.1778" layer="21"/>
<wire x1="10.949" y1="-1.29" x2="11.911" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="8.409" y1="1.25" x2="9.371" y2="1.25" width="0.1778" layer="21"/>
<wire x1="8.409" y1="-1.29" x2="9.371" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="5.869" y1="-1.29" x2="6.831" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="5.869" y1="1.25" x2="6.831" y2="1.25" width="0.1778" layer="21"/>
<wire x1="3.329" y1="-1.29" x2="4.291" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="3.329" y1="1.25" x2="4.291" y2="1.25" width="0.1778" layer="21"/>
<wire x1="0.789" y1="-1.29" x2="1.751" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="0.789" y1="1.25" x2="1.751" y2="1.25" width="0.1778" layer="21"/>
<smd name="5" x="10.16" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="3" x="5.08" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1" x="0" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="6" x="12.7" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="4" x="7.62" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="2" x="2.54" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1-2" x="0" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="2-2" x="2.54" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="3-2" x="5.08" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="4-2" x="7.62" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="5-2" x="10.16" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="6-2" x="12.7" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<text x="0" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="-4.191" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="BTA">
<description>British Telecom connector, used for Vernier sensors (analog)</description>
<hole x="6.45" y="0" drill="3.2"/>
<hole x="-6.45" y="0" drill="3.2"/>
<pad name="1" x="4.75" y="9" drill="1.4" diameter="2.54" shape="square" rot="R90"/>
<pad name="2" x="3.25" y="6" drill="1.4" diameter="2.54"/>
<pad name="3" x="1.75" y="9" drill="1.4" diameter="2.54"/>
<pad name="5" x="-1.25" y="9" drill="1.4" diameter="2.54"/>
<pad name="4" x="0.25" y="6" drill="1.4" diameter="2.54"/>
<pad name="6" x="-2.75" y="6" drill="1.4" diameter="2.54"/>
<wire x1="-8" y1="-7.5" x2="-8" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="-3.8" x2="-7.95" y2="9.9" width="0.2032" layer="21"/>
<wire x1="-8" y1="-7.5" x2="7.9" y2="-7.5" width="0.2032" layer="21"/>
<wire x1="7.9" y1="-7.5" x2="7.9" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="7.95" y1="-3.8" x2="7.95" y2="9.9" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="9.9" x2="7.95" y2="9.9" width="0.2032" layer="21"/>
<wire x1="-8.925" y1="-3.8" x2="-8.925" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="8.925" y1="-3.8" x2="8.925" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="-8.925" y1="-5.1" x2="-8" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="7.9" y1="-5.1" x2="8.925" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="-8.925" y1="-3.8" x2="-7.95" y2="-3.8" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="-3.8" x2="7.95" y2="-3.8" width="0.2032" layer="21"/>
<wire x1="7.95" y1="-3.8" x2="8.925" y2="-3.8" width="0.2032" layer="21"/>
<text x="-2.92" y="-7.07" size="1.27" layer="25">&gt;NAME</text>
<wire x1="-8" y1="-5.1" x2="7.9" y2="-5.1" width="0.2032" layer="21"/>
</package>
<package name="BTD">
<description>British Telecom connector, used for Vernier sensors (digital)</description>
<hole x="6.45" y="0" drill="3.2"/>
<hole x="-6.45" y="0" drill="3.2"/>
<wire x1="-7.95" y1="-3.8" x2="-7.95" y2="9.9" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="-7.5" x2="7.95" y2="-7.5" width="0.2032" layer="21"/>
<wire x1="7.95" y1="-3.8" x2="7.95" y2="9.9" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="9.9" x2="7.95" y2="9.9" width="0.2032" layer="21"/>
<wire x1="-8.975" y1="-3.8" x2="-8.975" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="8.875" y1="-3.8" x2="8.875" y2="-5.1" width="0.2032" layer="21"/>
<pad name="2" x="-3.25" y="9" drill="1.4" diameter="2.54" rot="R180"/>
<pad name="1" x="-4.75" y="6" drill="1.4" diameter="2.54" shape="square" rot="R180"/>
<pad name="4" x="-0.25" y="9" drill="1.4" diameter="2.54" rot="R180"/>
<pad name="6" x="2.75" y="9" drill="1.4" diameter="2.54" rot="R180"/>
<pad name="3" x="-1.75" y="6" drill="1.4" diameter="2.54" rot="R180"/>
<pad name="5" x="1.25" y="6" drill="1.4" diameter="2.54" rot="R180"/>
<wire x1="-8.975" y1="-5.1" x2="-7.95" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="-5.1" x2="7.95" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="7.95" y1="-5.1" x2="8.875" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="-8.975" y1="-3.8" x2="-7.95" y2="-3.8" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="-3.8" x2="7.95" y2="-3.8" width="0.2032" layer="21"/>
<wire x1="7.95" y1="-7.5" x2="7.95" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="-7.95" y1="-7.5" x2="-7.95" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="7.95" y1="-3.8" x2="8.875" y2="-3.8" width="0.2032" layer="21"/>
<text x="-3" y="-7" size="1.27" layer="25">&gt;Name</text>
</package>
<package name="1X06_SMD_MALE">
<text x="-1" y="3.321" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1" y="-4.591" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-1.27" y1="1.25" x2="-1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="13.97" y2="-1.25" width="0.127" layer="51"/>
<wire x1="13.97" y1="-1.25" x2="13.97" y2="1.25" width="0.127" layer="51"/>
<wire x1="13.97" y1="1.25" x2="-1.27" y2="1.25" width="0.127" layer="51"/>
<circle x="0" y="0" radius="0.64" width="0.127" layer="51"/>
<circle x="2.54" y="0" radius="0.64" width="0.127" layer="51"/>
<circle x="5.08" y="0" radius="0.64" width="0.127" layer="51"/>
<circle x="7.62" y="0" radius="0.64" width="0.127" layer="51"/>
<circle x="10.16" y="0" radius="0.64" width="0.127" layer="51"/>
<circle x="12.7" y="0" radius="0.64" width="0.127" layer="51"/>
<rectangle x1="-0.32" y1="0" x2="0.32" y2="2.75" layer="51"/>
<rectangle x1="4.76" y1="0" x2="5.4" y2="2.75" layer="51"/>
<rectangle x1="9.84" y1="0" x2="10.48" y2="2.75" layer="51"/>
<rectangle x1="2.22" y1="-2.75" x2="2.86" y2="0" layer="51" rot="R180"/>
<rectangle x1="7.3" y1="-2.75" x2="7.94" y2="0" layer="51" rot="R180"/>
<rectangle x1="12.38" y1="-2.75" x2="13.02" y2="0" layer="51" rot="R180"/>
<smd name="1" x="0" y="0" dx="1.02" dy="6" layer="1"/>
<smd name="2" x="2.54" y="0" dx="1.02" dy="6" layer="1"/>
<smd name="3" x="5.08" y="0" dx="1.02" dy="6" layer="1"/>
<smd name="4" x="7.62" y="0" dx="1.02" dy="6" layer="1"/>
<smd name="5" x="10.16" y="0" dx="1.02" dy="6" layer="1"/>
<smd name="6" x="12.7" y="0" dx="1.02" dy="6" layer="1"/>
<wire x1="-1.27" y1="1.25" x2="-1.27" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="-1.27" y1="-1.25" x2="-0.635" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="-1.27" y1="1.25" x2="-0.635" y2="1.25" width="0.1778" layer="21"/>
<wire x1="0.762" y1="1.25" x2="1.778" y2="1.25" width="0.1778" layer="21"/>
<wire x1="3.302" y1="1.25" x2="4.318" y2="1.25" width="0.1778" layer="21"/>
<wire x1="5.842" y1="1.25" x2="6.858" y2="1.25" width="0.1778" layer="21"/>
<wire x1="8.382" y1="1.25" x2="9.398" y2="1.25" width="0.1778" layer="21"/>
<wire x1="10.922" y1="1.25" x2="11.938" y2="1.25" width="0.1778" layer="21"/>
<wire x1="1.778" y1="-1.25" x2="0.762" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="4.318" y1="-1.25" x2="3.302" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="6.858" y1="-1.25" x2="5.842" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="9.398" y1="-1.25" x2="8.382" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="11.938" y1="-1.25" x2="10.922" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="13.97" y1="-1.25" x2="13.97" y2="1.25" width="0.1778" layer="21"/>
<wire x1="13.97" y1="-1.25" x2="13.335" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="13.97" y1="1.25" x2="13.335" y2="1.25" width="0.1778" layer="21"/>
</package>
<package name="1X06-1MM">
<description>Works with part #579 (https://www.sparkfun.com/products/579). Commonly used on GPS modules EM408, EM406 and GP-635T. Also compatible with cable 9123 (https://www.sparkfun.com/products/9123) and cable 574 (https://www.sparkfun.com/products/574).</description>
<circle x="-3.6" y="1.2" radius="0.1047" width="0.4064" layer="51"/>
<wire x1="-2.54" y1="-1.651" x2="2.54" y2="-1.651" width="0.254" layer="21"/>
<wire x1="-4.318" y1="0.508" x2="-4.318" y2="1.905" width="0.254" layer="21"/>
<wire x1="3.302" y1="1.905" x2="4.318" y2="1.905" width="0.254" layer="21"/>
<wire x1="4.318" y1="1.905" x2="4.318" y2="0.508" width="0.254" layer="21"/>
<wire x1="-4.318" y1="1.905" x2="-3.302" y2="1.905" width="0.254" layer="21"/>
<smd name="1" x="-2.54" y="1.27" dx="0.6" dy="1.55" layer="1"/>
<smd name="2" x="-1.54" y="1.27" dx="0.6" dy="1.55" layer="1"/>
<smd name="3" x="-0.54" y="1.27" dx="0.6" dy="1.55" layer="1"/>
<smd name="4" x="0.46" y="1.27" dx="0.6" dy="1.55" layer="1"/>
<smd name="5" x="1.46" y="1.27" dx="0.6" dy="1.55" layer="1"/>
<smd name="6" x="2.46" y="1.27" dx="0.6" dy="1.55" layer="1"/>
<smd name="P$1" x="-3.84" y="-0.955" dx="1.2" dy="1.8" layer="1"/>
<smd name="P$2" x="3.76" y="-0.955" dx="1.2" dy="1.8" layer="1"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="-2.54" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X06_NO_SILK">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X06_NO_SILK_YES_STOP">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="M06">
<wire x1="1.27" y1="-7.62" x2="-5.08" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="0" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-5.08" y1="10.16" x2="-5.08" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="1.27" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="10.16" x2="1.27" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="0" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="0" y2="7.62" width="0.6096" layer="94"/>
<text x="-5.08" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-5.08" y="10.922" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="5.08" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="5.08" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="5.08" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="5.08" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="5.08" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="M06" prefix="J" uservalue="yes">
<description>&lt;b&gt;Header 6&lt;/b&gt;&lt;br&gt;
Standard 6-pin 0.1" header. Use with straight break away headers (SKU : PRT-00116), right angle break away headers (PRT-00553), swiss pins (PRT-00743), machine pins (PRT-00117), and female headers (PRT-00115). Molex polarized connector foot print use with SKU : PRT-08094 with associated crimp pins and housings.&lt;p&gt;



NOTES ON THE VARIANTS LOCK and LOCK_LONGPADS...
This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place. You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers. Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<gates>
<gate name="G$1" symbol="M06" x="-2.54" y="0"/>
</gates>
<devices>
<device name="SILK_FEMALE_PTH" package="1X06">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08437"/>
</technology>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X6">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RA" package="MOLEX-1X6-RA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="1X06-SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08971" constant="no"/>
<attribute name="VALUE" value="RA 6Pin SMD" constant="no"/>
</technology>
</technologies>
</device>
<device name="LOCK" package="1X06_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X06_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X6_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RA_LOCK" package="MOLEX-1X6-RA_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SIP_LOCK" package="1X06-SIP_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FEMALE_LOCK" package="1X06_FEMALE_LOCK.010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LONGPADS" package="1X06_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1X06-SMD_EDGE_FEMALE" package="1X06-SMD_EDGE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM-6" package="SCREWTERMINAL-3.5MM-6">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMDF" package="1X06-SMD-FEMALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-FEMALE-V2" package="1X06-SMD-FEMALE-V2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09668"/>
</technology>
</technologies>
</device>
<device name="POGOPIN_HOLES_ONLY" package="1X06_HOLES_ONLY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08437"/>
</technology>
</technologies>
</device>
<device name="SMD-STRAIGHT-FEMALE" package="1X06_SMD_STRAIGHT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-10203"/>
</technology>
</technologies>
</device>
<device name="SMD-STRAIGHT-ALT-FEMALE" package="1X06_SMD_STRAIGHT_ALT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-10203"/>
</technology>
</technologies>
</device>
<device name="SMD-STRAIGHT-COMBO-FEMALE" package="1X06_SMD_STRAIGHT_COMBO">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-10203"/>
</technology>
</technologies>
</device>
<device name="VERNIER-ANALOG" package="BTA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VERNIER-DIGITAL" package="BTD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD_MALE" package="1X06_SMD_MALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-11293"/>
</technology>
</technologies>
</device>
<device name="SMD-1MM" package="1X06-1MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NO_SILK_FEMALE_PTH" package="1X06_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08437"/>
</technology>
</technologies>
</device>
<device name="NO_SILK_YES_STOP" package="1X06_NO_SILK_YES_STOP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Seeed-Switch-2016">
<packages>
<package name="SW4-SMD-4.2X3.2X2.5MM">
<wire x1="2.1" y1="1.6" x2="2.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="1.2" y1="-1.6" x2="-1.2" y2="-1.6" width="0.127" layer="21"/>
<wire x1="-2.1" y1="-1.6" x2="-2.1" y2="1.6" width="0.127" layer="51"/>
<wire x1="-1.2" y1="1.6" x2="1.2" y2="1.6" width="0.127" layer="21"/>
<smd name="1" x="-1.95" y="1.075" dx="1.3" dy="1.15" layer="1"/>
<smd name="3" x="-1.95" y="-1.075" dx="1.3" dy="1.15" layer="1"/>
<smd name="4" x="1.95" y="-1.075" dx="1.3" dy="1.15" layer="1"/>
<smd name="2" x="1.95" y="1.075" dx="1.3" dy="1.15" layer="1"/>
<text x="-1.905" y="1.905" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-1.905" y="-2.794" size="0.889" layer="27" ratio="11">&gt;VALUE</text>
<rectangle x1="-2.1" y1="-1.6" x2="2.1" y2="1.6" layer="39"/>
<wire x1="-2.1" y1="0.4" x2="-2.1" y2="-0.4" width="0.127" layer="21"/>
<wire x1="2.1" y1="-0.4" x2="2.1" y2="0.4" width="0.127" layer="21"/>
<wire x1="1.2" y1="1.6" x2="2.1" y2="1.6" width="0.127" layer="51"/>
<wire x1="-1.2" y1="1.6" x2="-2.1" y2="1.6" width="0.127" layer="51"/>
<wire x1="-1.2" y1="-1.6" x2="-2.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-1.3" y1="0" x2="-0.4" y2="1.1" width="0.127" layer="21" curve="-90"/>
<wire x1="-0.4" y1="1.1" x2="0.4" y2="1.1" width="0.127" layer="21"/>
<wire x1="0.4" y1="1.1" x2="1.3" y2="0" width="0.127" layer="21" curve="-90"/>
<wire x1="1.3" y1="0" x2="0.4" y2="-1.1" width="0.127" layer="21" curve="-90"/>
<wire x1="0.4" y1="-1.1" x2="-0.4" y2="-1.1" width="0.127" layer="21"/>
<wire x1="-0.4" y1="-1.1" x2="-1.3" y2="0" width="0.127" layer="21" curve="-90"/>
</package>
</packages>
<symbols>
<symbol name="BOTTON-4P">
<wire x1="-5.08" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<text x="-5.08" y="5.08" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-6.35" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="A0" x="-8.89" y="2.54" length="short"/>
<pin name="A1" x="8.89" y="2.54" length="short" rot="R180"/>
<pin name="B0" x="-8.89" y="-2.54" length="short"/>
<pin name="B1" x="8.89" y="-2.54" length="short" rot="R180"/>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="2.54" x2="-5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="-2.54" x2="-5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="6.35" y1="-2.54" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="6.35" y1="2.54" x2="5.08" y2="2.54" width="0.1524" layer="94"/>
<circle x="-1.27" y="-0.762" radius="0.1524" width="0" layer="94"/>
<circle x="0" y="-1.27" radius="0.1524" width="0" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SMD-BUTTON-TOP-DHT-4235A(4P-4.2X3.2MM)" prefix="SW" uservalue="yes">
<description>311020045</description>
<gates>
<gate name="G$1" symbol="BOTTON-4P" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SW4-SMD-4.2X3.2X2.5MM">
<connects>
<connect gate="G$1" pin="A0" pad="1"/>
<connect gate="G$1" pin="A1" pad="2"/>
<connect gate="G$1" pin="B0" pad="3"/>
<connect gate="G$1" pin="B1" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="DHT-4235A" constant="no"/>
<attribute name="VALUE" value="DHT-4235A" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Seeed-Resistor-2016">
<packages>
<package name="R0603">
<wire x1="0.635" y1="1.397" x2="0.635" y2="-1.397" width="0.127" layer="21"/>
<wire x1="0.635" y1="-1.397" x2="-0.635" y2="-1.397" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-1.397" x2="-0.635" y2="1.397" width="0.127" layer="21"/>
<wire x1="-0.635" y1="1.397" x2="0.635" y2="1.397" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.762" dx="0.889" dy="0.889" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.762" dx="0.889" dy="0.889" layer="1" roundness="25" rot="R270"/>
<text x="-1.016" y="-1.905" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="1.435" y="-1.605" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="RES">
<wire x1="-1.27" y1="0.508" x2="1.27" y2="0.508" width="0.254" layer="94"/>
<wire x1="1.27" y1="0.508" x2="1.27" y2="-0.508" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.508" x2="-1.27" y2="-0.508" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-0.508" x2="-1.27" y2="0.508" width="0.254" layer="94"/>
<text x="-3.81" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-2.54" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="1" x="-3.81" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="3.81" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SMD-RES-4.7K-5%-1/10W(0603)" prefix="R" uservalue="yes">
<description>301010290</description>
<gates>
<gate name="G$1" symbol="RES" x="0" y="0"/>
</gates>
<devices>
<device name="" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="RC0603JR-074K7L" constant="no"/>
<attribute name="VALUE" value="4.7K"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Seeed-Capacitor-2016">
<packages>
<package name="C0805">
<description>&lt;b&gt;0805&lt;b&gt;&lt;p&gt;</description>
<wire x1="0.889" y1="-1.651" x2="-0.889" y2="-1.651" width="0.127" layer="21"/>
<wire x1="-0.889" y1="-1.651" x2="-0.889" y2="1.651" width="0.127" layer="21"/>
<wire x1="-0.889" y1="1.651" x2="0.889" y2="1.651" width="0.127" layer="21"/>
<wire x1="0.889" y1="1.651" x2="0.889" y2="-1.651" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.889" dx="1.016" dy="1.397" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.889" dx="1.016" dy="1.397" layer="1" roundness="25" rot="R270"/>
<text x="1.143" y="1.905" size="0.889" layer="25" font="vector" ratio="11" rot="R270">&gt;NAME</text>
<text x="-2.159" y="1.905" size="0.635" layer="27" ratio="10" rot="R270">&gt;VALUE</text>
<rectangle x1="-1.4605" y1="-0.762" x2="1.4605" y2="0.762" layer="39" rot="R270"/>
</package>
<package name="C0603">
<description>&lt;b&gt;0603&lt;b&gt;&lt;p&gt;</description>
<wire x1="0.635" y1="1.397" x2="0.635" y2="-1.397" width="0.127" layer="21"/>
<wire x1="0.635" y1="-1.397" x2="-0.635" y2="-1.397" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-1.397" x2="-0.635" y2="1.397" width="0.127" layer="21"/>
<wire x1="-0.635" y1="1.397" x2="0.635" y2="1.397" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.762" dx="0.889" dy="0.889" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.762" dx="0.889" dy="0.889" layer="1" roundness="25" rot="R270"/>
<text x="0.889" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.5875" y="1.27" size="0.635" layer="27" ratio="10" rot="R270">&gt;VALUE</text>
<text x="-0.254" y="1.27" size="0.635" layer="33" ratio="10" rot="R270">&gt;name</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
</package>
<package name="ELECTRO-SMD-C-6MM">
<description>&lt;b&gt;Aluminium Electrolytic Capacitor Package B&lt;/b&gt;</description>
<wire x1="-3.175" y1="3.175" x2="2.413" y2="3.175" width="0.2032" layer="21"/>
<wire x1="2.413" y1="3.175" x2="3.175" y2="2.413" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-2.413" x2="2.413" y2="-3.175" width="0.2032" layer="21"/>
<wire x1="2.413" y1="-3.175" x2="-3.175" y2="-3.175" width="0.2032" layer="21"/>
<wire x1="-2.7432" y1="1.1176" x2="2.7432" y2="1.1176" width="0.2032" layer="21" curve="-135.667308"/>
<wire x1="2.7432" y1="-1.1176" x2="-2.7432" y2="-1.1176" width="0.2032" layer="21" curve="-135.667308"/>
<wire x1="-3.175" y1="3.175" x2="-3.175" y2="1.143" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="-3.175" x2="-3.175" y2="-1.143" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-2.413" x2="3.175" y2="-1.143" width="0.2032" layer="21"/>
<wire x1="3.175" y1="2.413" x2="3.175" y2="1.143" width="0.2032" layer="21"/>
<smd name="-" x="-2.667" y="0" dx="3.048" dy="1.651" layer="1"/>
<smd name="+" x="2.667" y="0" dx="3.048" dy="1.651" layer="1"/>
<text x="-1.905" y="3.556" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-2.54" y="-4.318" size="0.635" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.175" y1="-3.175" x2="3.175" y2="3.175" layer="39"/>
</package>
<package name="AVX-A">
<wire x1="-2.0955" y1="0.8255" x2="2.0955" y2="0.8255" width="0.127" layer="21"/>
<wire x1="2.0955" y1="0.8255" x2="2.0955" y2="-0.8255" width="0.127" layer="21"/>
<wire x1="2.0955" y1="-0.8255" x2="-2.0955" y2="-0.8255" width="0.127" layer="21"/>
<wire x1="-2.0955" y1="-0.8255" x2="-2.0955" y2="0.8255" width="0.127" layer="21"/>
<wire x1="-0.4445" y1="0" x2="0.0635" y2="0" width="0.127" layer="21"/>
<wire x1="-0.1905" y1="-0.254" x2="-0.1905" y2="0.254" width="0.127" layer="21"/>
<smd name="+" x="-1.27" y="0" dx="1.27" dy="1.27" layer="1"/>
<smd name="-" x="1.27" y="0" dx="1.27" dy="1.27" layer="1" rot="R180"/>
<text x="-1.905" y="1.016" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-1.905" y="-2.159" size="0.635" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.159" y1="-0.889" x2="2.159" y2="0.889" layer="39"/>
</package>
</packages>
<symbols>
<symbol name="C">
<wire x1="-0.635" y1="-1.016" x2="-0.635" y2="0" width="0.254" layer="94"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="1.016" width="0.254" layer="94"/>
<wire x1="0.635" y1="1.016" x2="0.635" y2="0" width="0.254" layer="94"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="0.635" y1="0" x2="1.27" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-2.54" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="1" x="-3.81" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="3.81" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="CAP-POLAR">
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="-2.54" width="0.508" layer="94"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="-2.54" width="0.508" layer="94" curve="47.924978"/>
<wire x1="1.27" y1="0" x2="0.635" y2="0" width="0.1524" layer="94"/>
<text x="-2.54" y="0" size="1.27" layer="93" ratio="10">+</text>
<text x="-6.35" y="3.81" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="1.27" y="3.81" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<text x="1.27" y="0" size="1.27" layer="93" ratio="10">-</text>
<pin name="+" x="-3.81" y="0" visible="off" length="short" direction="pas"/>
<pin name="-" x="3.81" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CERAMIC-100NF-50V-10%-X7R(0805)" prefix="C" uservalue="yes">
<description>302010165</description>
<gates>
<gate name="G$1" symbol="C" x="0" y="0"/>
</gates>
<devices>
<device name="" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="CC0805KRX7R9BB104" constant="no"/>
<attribute name="VALUE" value="100nf"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CERAMIC-100NF-50V-10%-X7R(0603)" prefix="C" uservalue="yes">
<description>302010138</description>
<gates>
<gate name="G$1" symbol="C" x="0" y="0"/>
</gates>
<devices>
<device name="" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="CC0603KRX7R9BB104" constant="no"/>
<attribute name="VALUE" value="100nf"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ALUMINUM-SMD-47UF-25V(D6.3-H5.3MM)" prefix="CT" uservalue="yes">
<description>302030059</description>
<gates>
<gate name="G$1" symbol="CAP-POLAR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ELECTRO-SMD-C-6MM">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="CS1E470M-CRE54"/>
<attribute name="VALUE" value="47UF"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TANTALUM-SMD-4.7UF-16V(AVX-A)" prefix="C" uservalue="yes">
<description>302020006</description>
<gates>
<gate name="G$1" symbol="CAP-POLAR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="AVX-A">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="TAJA475*016#NJ" constant="no"/>
<attribute name="VALUE" value="4.7uf"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Seeed-Crystal Oscillator-2016">
<packages>
<package name="X3-SMD-3.6X1.3X0.8MM">
<wire x1="1.7" y1="0.65" x2="1.8" y2="0.65" width="0.2032" layer="21"/>
<wire x1="1.8" y1="0.65" x2="1.8" y2="-0.65" width="0.2032" layer="21"/>
<wire x1="1.8" y1="-0.65" x2="1.7" y2="-0.65" width="0.2032" layer="21"/>
<wire x1="-1.7" y1="-0.65" x2="-1.85" y2="-0.65" width="0.2032" layer="21"/>
<wire x1="-1.85" y1="-0.65" x2="-1.85" y2="0.65" width="0.2032" layer="21"/>
<wire x1="-1.85" y1="0.65" x2="-1.7" y2="0.65" width="0.2032" layer="21"/>
<wire x1="-1.6" y1="0.65" x2="1.6" y2="0.65" width="0.127" layer="51"/>
<wire x1="1.6" y1="0.65" x2="1.6" y2="-0.65" width="0.127" layer="51"/>
<wire x1="1.6" y1="-0.65" x2="-1.6" y2="-0.65" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-0.65" x2="-1.6" y2="0.65" width="0.127" layer="51"/>
<smd name="1" x="-1.2" y="0" dx="0.7" dy="1.7" layer="1"/>
<smd name="2" x="0" y="0" dx="0.7" dy="1.7" layer="1"/>
<smd name="3" x="1.2" y="0" dx="0.7" dy="1.7" layer="1"/>
<text x="-1.905" y="1.27" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-1.905" y="-2.332" size="0.889" layer="27" ratio="11">&gt;VALUE</text>
<rectangle x1="-1.905" y1="-0.889" x2="1.778" y2="0.889" layer="39"/>
<wire x1="-0.8" y1="0.65" x2="-0.4" y2="0.65" width="0.127" layer="21"/>
<wire x1="0.4" y1="0.65" x2="0.8" y2="0.65" width="0.127" layer="21"/>
<wire x1="-0.8" y1="-0.65" x2="-0.4" y2="-0.65" width="0.127" layer="21"/>
<wire x1="0.4" y1="-0.65" x2="0.8" y2="-0.65" width="0.127" layer="21"/>
<circle x="-1.4" y="-1.2" radius="0.2" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="CRYSTAL-RESONATOR">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.1524" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="1.016" y1="0" x2="1.016" y2="-1.778" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="0" x2="-1.016" y2="-1.778" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="3.302" y1="-2.032" x2="2.54" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.032" x2="1.778" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="3.302" y1="-3.048" x2="2.54" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-3.048" x2="1.778" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-3.048" x2="2.54" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="-3.048" x2="-2.54" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-3.048" x2="-3.302" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-3.048" x2="-2.54" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="-2.032" x2="-2.54" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.032" x2="-3.302" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.032" x2="-2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="0" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="0" y1="-5.08" x2="-2.54" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="0" y1="-5.08" x2="0" y2="-7.62" width="0.1524" layer="94"/>
<text x="-2.54" y="2.54" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="2.54" y="-7.62" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="3" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="2" x="0" y="-7.62" visible="off" length="point" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SMD-RESONATOR-16MHZ-15PF-0.5%-40R(3P-3.2X1.3MM)" prefix="Y" uservalue="yes">
<description>306030002</description>
<gates>
<gate name="G$1" symbol="CRYSTAL-RESONATOR" x="0" y="2.54"/>
</gates>
<devices>
<device name="" package="X3-SMD-3.6X1.3X0.8MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="CSTCE16M0V53-R0" constant="no"/>
<attribute name="VALUE" value="16MHZ" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Bluetooth%20HC-05">
<description>Bluetooth HC05 Module. See http://mcuoneclipse.com/2013/02/14/bluetooth-with-the-freedom-board/</description>
<packages>
<package name="BLUETOOTH_SMD">
<description>HC-05</description>
<wire x1="-6.35" y1="4.81" x2="-6.35" y2="13.31" width="0.127" layer="21"/>
<wire x1="-6.35" y1="13.31" x2="6.65" y2="13.31" width="0.127" layer="21"/>
<wire x1="6.65" y1="13.31" x2="6.65" y2="4.81" width="0.127" layer="21"/>
<smd name="1" x="-6.35" y="3.81" dx="2" dy="1" layer="1"/>
<smd name="2" x="-6.35" y="2.31" dx="2" dy="1" layer="1"/>
<smd name="3" x="-6.35" y="0.81" dx="2" dy="1" layer="1"/>
<smd name="4" x="-6.35" y="-0.69" dx="2" dy="1" layer="1"/>
<smd name="5" x="-6.35" y="-2.19" dx="2" dy="1" layer="1"/>
<smd name="6" x="-6.35" y="-3.69" dx="2" dy="1" layer="1"/>
<smd name="7" x="-6.35" y="-5.19" dx="2" dy="1" layer="1"/>
<smd name="8" x="-6.35" y="-6.69" dx="2" dy="1" layer="1"/>
<smd name="9" x="-6.35" y="-8.19" dx="2" dy="1" layer="1"/>
<smd name="10" x="-6.35" y="-9.69" dx="2" dy="1" layer="1"/>
<smd name="11" x="-6.35" y="-11.19" dx="2" dy="1" layer="1"/>
<smd name="12" x="-6.35" y="-12.69" dx="2" dy="1" layer="1"/>
<smd name="13" x="-6.35" y="-14.19" dx="2" dy="1" layer="1"/>
<smd name="22" x="6.65" y="-14.19" dx="2" dy="1" layer="1"/>
<smd name="23" x="6.65" y="-12.69" dx="2" dy="1" layer="1"/>
<smd name="24" x="6.65" y="-11.19" dx="2" dy="1" layer="1"/>
<smd name="25" x="6.65" y="-9.69" dx="2" dy="1" layer="1"/>
<smd name="26" x="6.65" y="-8.19" dx="2" dy="1" layer="1"/>
<smd name="27" x="6.65" y="-6.69" dx="2" dy="1" layer="1"/>
<smd name="28" x="6.65" y="-5.19" dx="2" dy="1" layer="1"/>
<smd name="29" x="6.65" y="-3.69" dx="2" dy="1" layer="1"/>
<smd name="30" x="6.65" y="-2.19" dx="2" dy="1" layer="1"/>
<smd name="31" x="6.65" y="-0.69" dx="2" dy="1" layer="1"/>
<smd name="32" x="6.65" y="0.81" dx="2" dy="1" layer="1"/>
<smd name="33" x="6.65" y="2.31" dx="2" dy="1" layer="1"/>
<smd name="34" x="6.65" y="3.81" dx="2" dy="1" layer="1"/>
<smd name="14" x="-5.1" y="-15.94" dx="1.8" dy="1" layer="1" rot="R90"/>
<smd name="15" x="-3.6" y="-15.94" dx="1.8" dy="1" layer="1" rot="R90"/>
<smd name="16" x="-2.1" y="-15.94" dx="1.8" dy="1" layer="1" rot="R90"/>
<smd name="17" x="-0.6" y="-15.94" dx="1.8" dy="1" layer="1" rot="R90"/>
<smd name="18" x="0.9" y="-15.94" dx="1.8" dy="1" layer="1" rot="R90"/>
<smd name="19" x="2.4" y="-15.94" dx="1.8" dy="1" layer="1" rot="R90"/>
<smd name="20" x="3.9" y="-15.94" dx="1.8" dy="1" layer="1" rot="R90"/>
<smd name="21" x="5.4" y="-15.94" dx="1.8" dy="1" layer="1" rot="R90"/>
<text x="-5.35" y="11.31" size="1.27" layer="25">&gt;name</text>
</package>
</packages>
<symbols>
<symbol name="HC-05">
<wire x1="-17.78" y1="17.78" x2="-17.78" y2="-17.78" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-17.78" x2="20.32" y2="-17.78" width="0.254" layer="94"/>
<wire x1="20.32" y1="-17.78" x2="20.32" y2="17.78" width="0.254" layer="94"/>
<wire x1="20.32" y1="17.78" x2="-17.78" y2="17.78" width="0.254" layer="94"/>
<text x="-17.78" y="22.86" size="1.778" layer="95">&gt;NAME</text>
<text x="-17.78" y="20.32" size="1.778" layer="96">&gt;VALUE</text>
<pin name="TX" x="-22.86" y="15.24" length="middle"/>
<pin name="RX" x="-22.86" y="12.7" length="middle"/>
<pin name="CTS" x="-22.86" y="10.16" length="middle"/>
<pin name="RTS" x="-22.86" y="7.62" length="middle"/>
<pin name="PCM_CLK" x="-22.86" y="5.08" length="middle"/>
<pin name="PCM_OUT" x="-22.86" y="2.54" length="middle"/>
<pin name="PCM_IN" x="-22.86" y="0" length="middle"/>
<pin name="PCN_SYNC" x="-22.86" y="-2.54" length="middle"/>
<pin name="AIO0" x="-22.86" y="-5.08" length="middle"/>
<pin name="AIO1" x="-22.86" y="-7.62" length="middle"/>
<pin name="RST" x="-22.86" y="-10.16" length="middle"/>
<pin name="3V3" x="-22.86" y="-12.7" length="middle"/>
<pin name="GND" x="-22.86" y="-15.24" length="middle"/>
<pin name="NC" x="-7.62" y="-22.86" length="middle" rot="R90"/>
<pin name="VBUSD-" x="-5.08" y="-22.86" length="middle" rot="R90"/>
<pin name="CSB" x="-2.54" y="-22.86" length="middle" rot="R90"/>
<pin name="MOSI" x="0" y="-22.86" length="middle" rot="R90"/>
<pin name="MISO" x="2.54" y="-22.86" length="middle" rot="R90"/>
<pin name="CLK" x="5.08" y="-22.86" length="middle" rot="R90"/>
<pin name="VBUSD+" x="7.62" y="-22.86" length="middle" rot="R90"/>
<pin name="GND1" x="10.16" y="-22.86" length="middle" rot="R90"/>
<pin name="GND2" x="25.4" y="-15.24" length="middle" rot="R180"/>
<pin name="PIO0" x="25.4" y="-12.7" length="middle" rot="R180"/>
<pin name="PIO1" x="25.4" y="-10.16" length="middle" rot="R180"/>
<pin name="PIO2" x="25.4" y="-7.62" length="middle" rot="R180"/>
<pin name="PIO3" x="25.4" y="-5.08" length="middle" rot="R180"/>
<pin name="PIO4" x="25.4" y="-2.54" length="middle" rot="R180"/>
<pin name="PIO5" x="25.4" y="0" length="middle" rot="R180"/>
<pin name="PIO6" x="25.4" y="2.54" length="middle" rot="R180"/>
<pin name="PIO7" x="25.4" y="5.08" length="middle" rot="R180"/>
<pin name="PIO8" x="25.4" y="7.62" length="middle" rot="R180"/>
<pin name="PIO9" x="25.4" y="10.16" length="middle" rot="R180"/>
<pin name="PIO10" x="25.4" y="12.7" length="middle" rot="R180"/>
<pin name="PIO11" x="25.4" y="15.24" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="HC_05" prefix="U">
<description>HC-05 Bluetooth Module</description>
<gates>
<gate name="G$1" symbol="HC-05" x="0" y="0"/>
</gates>
<devices>
<device name="" package="BLUETOOTH_SMD">
<connects>
<connect gate="G$1" pin="3V3" pad="12"/>
<connect gate="G$1" pin="AIO0" pad="9"/>
<connect gate="G$1" pin="AIO1" pad="10"/>
<connect gate="G$1" pin="CLK" pad="19"/>
<connect gate="G$1" pin="CSB" pad="16"/>
<connect gate="G$1" pin="CTS" pad="3"/>
<connect gate="G$1" pin="GND" pad="13"/>
<connect gate="G$1" pin="GND1" pad="21"/>
<connect gate="G$1" pin="GND2" pad="22"/>
<connect gate="G$1" pin="MISO" pad="18"/>
<connect gate="G$1" pin="MOSI" pad="17"/>
<connect gate="G$1" pin="NC" pad="14"/>
<connect gate="G$1" pin="PCM_CLK" pad="5"/>
<connect gate="G$1" pin="PCM_IN" pad="7"/>
<connect gate="G$1" pin="PCM_OUT" pad="6"/>
<connect gate="G$1" pin="PCN_SYNC" pad="8"/>
<connect gate="G$1" pin="PIO0" pad="23"/>
<connect gate="G$1" pin="PIO1" pad="24"/>
<connect gate="G$1" pin="PIO10" pad="33"/>
<connect gate="G$1" pin="PIO11" pad="34"/>
<connect gate="G$1" pin="PIO2" pad="25"/>
<connect gate="G$1" pin="PIO3" pad="26"/>
<connect gate="G$1" pin="PIO4" pad="27"/>
<connect gate="G$1" pin="PIO5" pad="28"/>
<connect gate="G$1" pin="PIO6" pad="29"/>
<connect gate="G$1" pin="PIO7" pad="30"/>
<connect gate="G$1" pin="PIO8" pad="31"/>
<connect gate="G$1" pin="PIO9" pad="32"/>
<connect gate="G$1" pin="RST" pad="11"/>
<connect gate="G$1" pin="RTS" pad="4"/>
<connect gate="G$1" pin="RX" pad="2"/>
<connect gate="G$1" pin="TX" pad="1"/>
<connect gate="G$1" pin="VBUSD+" pad="20"/>
<connect gate="G$1" pin="VBUSD-" pad="15"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Seeed-Transistor-2016">
<packages>
<package name="SOT-23">
<description>&lt;b&gt;SOT23&lt;/b&gt;</description>
<wire x1="-0.1905" y1="-0.635" x2="0.1905" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.4605" y1="-0.254" x2="1.4605" y2="0.635" width="0.127" layer="21"/>
<wire x1="1.4605" y1="0.635" x2="0.6985" y2="0.635" width="0.127" layer="21"/>
<wire x1="-0.6985" y1="0.635" x2="-1.4605" y2="0.635" width="0.127" layer="21"/>
<wire x1="-1.4605" y1="0.635" x2="-1.4605" y2="-0.254" width="0.127" layer="21"/>
<smd name="3" x="0" y="1.016" dx="1.016" dy="1.143" layer="1"/>
<smd name="2" x="0.889" y="-1.016" dx="1.016" dy="1.143" layer="1"/>
<smd name="1" x="-0.889" y="-1.016" dx="1.016" dy="1.143" layer="1" rot="R180"/>
<text x="-1.905" y="1.905" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-1.27" y="-2.794" size="0.635" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
<rectangle x1="-1.524" y1="-1.651" x2="1.524" y2="1.651" layer="39"/>
</package>
</packages>
<symbols>
<symbol name="MOSFET-N">
<wire x1="0" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.905" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="1.905" x2="1.5875" y2="1.905" width="0.254" layer="94"/>
<wire x1="1.5875" y1="-1.905" x2="0" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-1.905" y1="2.54" x2="-1.905" y2="1.905" width="0.254" layer="94"/>
<wire x1="-1.905" y1="1.905" x2="-1.905" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.905" y1="0.635" x2="-1.905" y2="0" width="0.254" layer="94"/>
<wire x1="-1.905" y1="0" x2="-1.905" y2="-0.635" width="0.254" layer="94"/>
<wire x1="-1.905" y1="-2.54" x2="-1.905" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-1.905" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-1.905" y1="1.905" x2="0" y2="1.905" width="0.254" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="0" y2="-1.905" width="0.254" layer="94"/>
<wire x1="1.5875" y1="-1.905" x2="1.5875" y2="0" width="0.254" layer="94"/>
<wire x1="1.5875" y1="0" x2="1.5875" y2="0.4445" width="0.254" layer="94"/>
<wire x1="1.5875" y1="0.4445" x2="1.5875" y2="1.905" width="0.254" layer="94"/>
<wire x1="2.2225" y1="-0.4445" x2="1.905" y2="-0.4445" width="0.254" layer="94"/>
<wire x1="1.905" y1="-0.4445" x2="1.27" y2="-0.4445" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.4445" x2="0.9525" y2="-0.4445" width="0.254" layer="94"/>
<wire x1="0.9525" y1="-0.4445" x2="1.5875" y2="0.4445" width="0.254" layer="94"/>
<wire x1="1.5875" y1="0.4445" x2="2.2225" y2="-0.4445" width="0.254" layer="94"/>
<wire x1="2.2225" y1="0.4445" x2="1.5875" y2="0.4445" width="0.254" layer="94"/>
<wire x1="1.5875" y1="0.4445" x2="0.9525" y2="0.4445" width="0.254" layer="94"/>
<wire x1="1.905" y1="-0.4445" x2="1.5875" y2="0" width="0.254" layer="94"/>
<wire x1="1.5875" y1="0" x2="1.27" y2="-0.4445" width="0.254" layer="94"/>
<wire x1="1.905" y1="-0.3175" x2="1.5875" y2="0" width="0.254" layer="94"/>
<wire x1="1.5875" y1="0" x2="1.27" y2="-0.3175" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.5875" y2="0" width="0.254" layer="94"/>
<wire x1="-1.905" y1="0" x2="-0.9525" y2="-0.3175" width="0.254" layer="94"/>
<wire x1="-1.905" y1="0" x2="-0.9525" y2="0.3175" width="0.254" layer="94"/>
<wire x1="-0.9525" y1="-0.3175" x2="-0.9525" y2="0.3175" width="0.254" layer="94"/>
<wire x1="-0.9525" y1="0.3175" x2="-1.5875" y2="0" width="0.254" layer="94"/>
<wire x1="-1.5875" y1="0" x2="-0.9525" y2="-0.3175" width="0.254" layer="94"/>
<wire x1="-0.9525" y1="-0.3175" x2="-1.397" y2="0.127" width="0.254" layer="94"/>
<circle x="0" y="1.905" radius="0.254" width="0.254" layer="94"/>
<circle x="0" y="-1.905" radius="0.254" width="0.254" layer="94"/>
<text x="-3.81" y="2.54" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="0" y="2.54" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<text x="-0.635" y="2.2225" size="0.8128" layer="93">D</text>
<text x="-0.635" y="-3.175" size="0.8128" layer="93">S</text>
<text x="-3.4925" y="0" size="0.8128" layer="93">G</text>
<pin name="G" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="D" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="S" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SMD-MOSFET-N-CH-20V-2.1A-CJ2302(SOT-23)" prefix="Q" uservalue="yes">
<description>305030015</description>
<gates>
<gate name="G$1" symbol="MOSFET-N" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT-23">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="CJ2302" constant="no"/>
<attribute name="VALUE" value="CJ2302" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Retired">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find all manner of retired footprints for resistors, capacitors, board names, ICs, etc., that are no longer used in our catalog.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt;Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="SJ_2S">
<description>Small solder jumper with big paste layer so it will short during reflow.</description>
<wire x1="0.8" y1="-1" x2="-0.8" y2="-1" width="0.1524" layer="21"/>
<wire x1="0.8" y1="1" x2="1.1" y2="0.75" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.1" y1="0.75" x2="-0.8" y2="1" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.1" y1="-0.75" x2="-0.8" y2="-1" width="0.1524" layer="21" curve="90.114706"/>
<wire x1="0.8" y1="-1" x2="1.1" y2="-0.75" width="0.1524" layer="21" curve="90"/>
<wire x1="1.1" y1="-0.75" x2="1.1" y2="0.75" width="0.1524" layer="21"/>
<wire x1="-1.1" y1="-0.75" x2="-1.1" y2="0.75" width="0.1524" layer="21"/>
<wire x1="-0.8" y1="1" x2="0.8" y2="1" width="0.1524" layer="21"/>
<smd name="1" x="-0.4119" y="0" dx="0.635" dy="1.27" layer="1"/>
<smd name="2" x="0.4119" y="0" dx="0.635" dy="1.27" layer="1"/>
<text x="-0.9498" y="1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.9498" y="-1.651" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.2192" y1="-1.143" x2="1.2192" y2="1.143" layer="31"/>
</package>
<package name="SJ_2S-TRACE">
<description>Solder jumper, small, shorted with trace. No paste layer. Trace is cuttable.</description>
<wire x1="0.8255" y1="-1.016" x2="-0.8255" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="0.8255" y1="1.016" x2="1.0795" y2="0.762" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1.0795" y1="0.762" x2="-0.8255" y2="1.016" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1.0795" y1="-0.762" x2="-0.8255" y2="-1.016" width="0.2032" layer="21" curve="90"/>
<wire x1="0.8255" y1="-1.016" x2="1.0795" y2="-0.762" width="0.2032" layer="21" curve="90"/>
<wire x1="-0.8255" y1="1.016" x2="0.8255" y2="1.016" width="0.2032" layer="21"/>
<wire x1="-0.381" y1="0" x2="0.381" y2="0" width="0.2032" layer="1"/>
<smd name="1" x="-0.508" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
<smd name="2" x="0.508" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
<text x="-0.9525" y="1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.9525" y="-1.651" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="SJ_2S-NOTRACE">
<wire x1="0.8" y1="-1" x2="-0.8" y2="-1" width="0.2032" layer="21"/>
<wire x1="0.8" y1="1" x2="1" y2="0.7" width="0.2032" layer="21" curve="-90.076445"/>
<wire x1="-1" y1="0.7" x2="-0.8" y2="1" width="0.2032" layer="21" curve="-90.03821"/>
<wire x1="-1" y1="-0.7" x2="-0.8" y2="-1" width="0.2032" layer="21" curve="90.03821"/>
<wire x1="0.8" y1="-1" x2="1" y2="-0.7" width="0.2032" layer="21" curve="90.03821"/>
<wire x1="-0.8" y1="1" x2="0.8" y2="1" width="0.2032" layer="21"/>
<smd name="1" x="-0.4009" y="0" dx="0.635" dy="1.27" layer="1" rot="R180" cream="no"/>
<smd name="2" x="0.4127" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
<text x="-0.9525" y="1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.9525" y="-1.651" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.016" y1="-1.016" x2="1.016" y2="1.016" layer="31"/>
</package>
<package name="SJ_2S-NO">
<description>Small solder jumper with no paste layer so it will open after reflow.</description>
<wire x1="0.8" y1="-1" x2="-0.8" y2="-1" width="0.2032" layer="21"/>
<wire x1="0.8" y1="1" x2="1" y2="0.7" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1" y1="0.7" x2="-0.8" y2="1" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1" y1="-0.7" x2="-0.8" y2="-1" width="0.2032" layer="21" curve="90"/>
<wire x1="0.8" y1="-1" x2="1" y2="-0.7" width="0.2032" layer="21" curve="90"/>
<wire x1="-0.8" y1="1" x2="0.8" y2="1" width="0.2032" layer="21"/>
<smd name="1" x="-0.45" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
<smd name="2" x="0.45" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
<text x="-0.908" y="1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.908" y="-1.651" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="SJ_2S-TRACE-PTH">
<pad name="1" x="-1.27" y="0" drill="1.016" diameter="1.778"/>
<pad name="2" x="1.27" y="0" drill="1.016" diameter="1.778"/>
<text x="-2.54" y="-1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="0" y="-1.27" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3048" y1="-1.27" x2="0.3048" y2="1.27" layer="30"/>
<smd name="P$1" x="0" y="0" dx="1.27" dy="0.381" layer="16" rot="R180"/>
</package>
<package name="SJ_2S-PTH">
<pad name="1" x="-1.27" y="0" drill="1.016" diameter="1.778"/>
<pad name="2" x="1.27" y="0" drill="1.016" diameter="1.778"/>
<text x="-2.54" y="-1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="0" y="-1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="SJ_ROUND">
<smd name="1" x="-1.27" y="0" dx="0.3048" dy="0.1524" layer="1" roundness="20" rot="R270" stop="no" thermals="no" cream="no"/>
<smd name="2" x="1.27" y="0" dx="0.3048" dy="0.1524" layer="1" roundness="20" rot="R90" stop="no" thermals="no" cream="no"/>
<text x="-2.8575" y="2.2225" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-3.4925" size="1.27" layer="27">&gt;VALUE</text>
<polygon width="0" layer="1">
<vertex x="0.111125" y="-1.42875" curve="85"/>
<vertex x="1.381125" y="0" curve="85"/>
<vertex x="0.111125" y="1.42875"/>
</polygon>
<polygon width="0" layer="1">
<vertex x="-0.111125" y="1.42875"/>
<vertex x="-0.111125" y="-1.42875" curve="-85"/>
<vertex x="-1.381125" y="0" curve="-85"/>
</polygon>
<polygon width="0.2032" layer="29">
<vertex x="0" y="1.42875" curve="-90"/>
<vertex x="1.42875" y="0" curve="-90"/>
<vertex x="0" y="-1.42875" curve="-90"/>
<vertex x="-1.42875" y="0" curve="-90"/>
</polygon>
<circle x="0" y="0" radius="1.74625" width="0.2032" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="SJ">
<wire x1="0.381" y1="0.635" x2="0.381" y2="-0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="2.54" y1="0" x2="1.651" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.651" y2="0" width="0.1524" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SOLDERJUMPER" prefix="SJ">
<description>&lt;b&gt;Solder Jumper&lt;/b&gt;
Standard SMD solder jumper. Used to automate production. Two varients : Normally Open and Normally Closed are the same, but have different paste layers. NC will have a large amount of paste and should jumper during reflow.</description>
<gates>
<gate name="1" symbol="SJ" x="0" y="0"/>
</gates>
<devices>
<device name="NC" package="SJ_2S">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TRACE" package="SJ_2S-TRACE">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NC2" package="SJ_2S-NOTRACE">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NO" package="SJ_2S-NO">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TRACE-PTH" package="SJ_2S-TRACE-PTH">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH" package="SJ_2S-PTH">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ROUND" package="SJ_ROUND">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Seeed-Sensor-2016">
<packages>
<package name="SNR2-3.0">
<wire x1="2.4384" y1="0.4064" x2="2.4384" y2="-0.4064" width="0.127" layer="21"/>
<wire x1="2.4384" y1="-0.4064" x2="0.6604" y2="-2.1844" width="0.127" layer="21" curve="-90"/>
<wire x1="0.6604" y1="-2.1844" x2="-0.6604" y2="-2.1844" width="0.127" layer="21"/>
<wire x1="-0.6604" y1="-2.1844" x2="-2.4384" y2="-0.4064" width="0.127" layer="21" curve="-90"/>
<wire x1="-2.4384" y1="-0.4064" x2="-2.4384" y2="0.4064" width="0.127" layer="21"/>
<wire x1="-2.4384" y1="0.4064" x2="-0.6604" y2="2.1844" width="0.127" layer="21" curve="-90"/>
<wire x1="-0.6604" y1="2.1844" x2="0.6604" y2="2.1844" width="0.127" layer="21"/>
<wire x1="0.6604" y1="2.1844" x2="2.4384" y2="0.4064" width="0.127" layer="21" curve="-90"/>
<pad name="1" x="-1.651" y="0" drill="0.762" diameter="1.397" shape="square"/>
<pad name="2" x="1.651" y="0" drill="0.762" diameter="1.397"/>
<text x="-2.54" y="2.54" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="0.889" layer="27" ratio="11">&gt;VALUE</text>
<rectangle x1="-2.4384" y1="-2.1844" x2="2.4384" y2="2.1844" layer="39"/>
</package>
</packages>
<symbols>
<symbol name="PHOTORESISTOR-5528-10K(2P-5.0X4.2MM)">
<wire x1="-3.81" y1="1.27" x2="3.81" y2="1.27" width="0.1524" layer="94"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-1.27" x2="-3.81" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="-1.27" x2="-3.81" y2="1.27" width="0.1524" layer="94"/>
<text x="-8.89" y="0" size="1.27" layer="104" ratio="10">&gt;NAME</text>
<text x="-1.27" y="3.81" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="P$1" x="-6.35" y="0" visible="off" length="short"/>
<pin name="P$2" x="6.35" y="0" visible="off" length="short" rot="R180"/>
<wire x1="-1.905" y1="3.175" x2="-0.635" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="3.175" x2="1.27" y2="1.905" width="0.1524" layer="94"/>
<wire x1="-0.635" y1="1.905" x2="-0.635" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-0.635" y1="1.905" x2="-1.27" y2="1.905" width="0.1524" layer="94"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="1.905" x2="0.635" y2="1.905" width="0.1524" layer="94"/>
<text x="-7.62" y="3.81" size="1.27" layer="95" ratio="10">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="PHOTORESISTOR-5528-10K(2P-5.0X4.2MM)" prefix="U" uservalue="yes">
<description>314020017</description>
<gates>
<gate name="G$1" symbol="PHOTORESISTOR-5528-10K(2P-5.0X4.2MM)" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SNR2-3.0">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="PHOTORESISTOR-5528-10K" constant="no"/>
<attribute name="VALUE" value="PHOTORESISTOR-5528-10K" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="D1" library="Seeed-OPL-2017-01-Diode-james" deviceset="SMD-RGB-LED-4P" device="'WS2812B'"/>
<part name="SUPPLY8" library="supply2" deviceset="GND" device=""/>
<part name="D2" library="Seeed-OPL-2017-01-Diode-james" deviceset="SMD-DIODE-SCHOTTKY-28V-5A(DO-214AA)" device="" value="28V-5A"/>
<part name="F1" library="Seeed-OPL-2017-01-Fuse-Akita" deviceset="307010043_SMD_PPTC_2A_1210" device=""/>
<part name="USB1" library="Seeed-Connector -2016" deviceset="MICRO-USB-SMD-B-(10118193-0001LF)" device="" value="10118193-0001LF"/>
<part name="U2" library="Seeed-IC-2016" deviceset="MCU-ATMEGA328P-MU(QFN32)" device="" value="ATMEGA328P-MU-QFN32"/>
<part name="J2" library="SparkFun-Connectors" deviceset="M06" device="SILK_FEMALE_PTH" value="FTDI"/>
<part name="SUPPLY1" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY2" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY3" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY4" library="supply2" deviceset="GND" device=""/>
<part name="SW1" library="Seeed-Switch-2016" deviceset="SMD-BUTTON-TOP-DHT-4235A(4P-4.2X3.2MM)" device="" value="DHT-4235A"/>
<part name="SUPPLY5" library="supply2" deviceset="GND" device=""/>
<part name="C1" library="Seeed-Capacitor-2016" deviceset="CERAMIC-100NF-50V-10%-X7R(0603)" device="" value="100nf"/>
<part name="Y1" library="Seeed-Crystal Oscillator-2016" deviceset="SMD-RESONATOR-16MHZ-15PF-0.5%-40R(3P-3.2X1.3MM)" device="" value="16MHZ"/>
<part name="SUPPLY9" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY10" library="supply2" deviceset="GND" device=""/>
<part name="C2" library="Seeed-Capacitor-2016" deviceset="CERAMIC-100NF-50V-10%-X7R(0603)" device="" value="100nf"/>
<part name="SUPPLY11" library="supply2" deviceset="GND" device=""/>
<part name="J4" library="SparkFun-Connectors" deviceset="M06" device="SILK_FEMALE_PTH" value="BOOT"/>
<part name="SUPPLY6" library="supply2" deviceset="GND" device=""/>
<part name="CT1" library="Seeed-Capacitor-2016" deviceset="ALUMINUM-SMD-47UF-25V(D6.3-H5.3MM)" device="" value="47UF"/>
<part name="SUPPLY7" library="supply2" deviceset="GND" device=""/>
<part name="C3" library="Seeed-Capacitor-2016" deviceset="TANTALUM-SMD-4.7UF-16V(AVX-A)" device="" value="4.7uf"/>
<part name="SUPPLY12" library="supply2" deviceset="GND" device=""/>
<part name="C4" library="Seeed-Capacitor-2016" deviceset="CERAMIC-100NF-50V-10%-X7R(0805)" device="" value="100nf"/>
<part name="C5" library="Seeed-Capacitor-2016" deviceset="CERAMIC-100NF-50V-10%-X7R(0805)" device="" value="100nf"/>
<part name="C6" library="Seeed-Capacitor-2016" deviceset="CERAMIC-100NF-50V-10%-X7R(0805)" device="" value="100nf"/>
<part name="C7" library="Seeed-Capacitor-2016" deviceset="CERAMIC-100NF-50V-10%-X7R(0805)" device="" value="100nf"/>
<part name="C8" library="Seeed-Capacitor-2016" deviceset="CERAMIC-100NF-50V-10%-X7R(0805)" device="" value="100nf"/>
<part name="C9" library="Seeed-Capacitor-2016" deviceset="CERAMIC-100NF-50V-10%-X7R(0805)" device="" value="100nf"/>
<part name="C10" library="Seeed-Capacitor-2016" deviceset="CERAMIC-100NF-50V-10%-X7R(0805)" device="" value="100nf"/>
<part name="C11" library="Seeed-Capacitor-2016" deviceset="CERAMIC-100NF-50V-10%-X7R(0805)" device="" value="100nf"/>
<part name="C12" library="Seeed-Capacitor-2016" deviceset="CERAMIC-100NF-50V-10%-X7R(0805)" device="" value="100nf"/>
<part name="C13" library="Seeed-Capacitor-2016" deviceset="CERAMIC-100NF-50V-10%-X7R(0805)" device="" value="100nf"/>
<part name="C14" library="Seeed-Capacitor-2016" deviceset="CERAMIC-100NF-50V-10%-X7R(0805)" device="" value="100nf"/>
<part name="C15" library="Seeed-Capacitor-2016" deviceset="CERAMIC-100NF-50V-10%-X7R(0805)" device="" value="100nf"/>
<part name="C24" library="Seeed-Capacitor-2016" deviceset="CERAMIC-100NF-50V-10%-X7R(0805)" device="" value="100nf"/>
<part name="C25" library="Seeed-Capacitor-2016" deviceset="CERAMIC-100NF-50V-10%-X7R(0805)" device="" value="100nf"/>
<part name="C26" library="Seeed-Capacitor-2016" deviceset="CERAMIC-100NF-50V-10%-X7R(0805)" device="" value="100nf"/>
<part name="C27" library="Seeed-Capacitor-2016" deviceset="CERAMIC-100NF-50V-10%-X7R(0805)" device="" value="100nf"/>
<part name="SUPPLY15" library="supply2" deviceset="GND" device=""/>
<part name="D3" library="Seeed-OPL-2017-01-Diode-james" deviceset="SMD-RGB-LED-4P" device="'WS2812B'"/>
<part name="SUPPLY13" library="supply2" deviceset="GND" device=""/>
<part name="D4" library="Seeed-OPL-2017-01-Diode-james" deviceset="SMD-RGB-LED-4P" device="'WS2812B'"/>
<part name="SUPPLY14" library="supply2" deviceset="GND" device=""/>
<part name="D5" library="Seeed-OPL-2017-01-Diode-james" deviceset="SMD-RGB-LED-4P" device="'WS2812B'"/>
<part name="SUPPLY17" library="supply2" deviceset="GND" device=""/>
<part name="D6" library="Seeed-OPL-2017-01-Diode-james" deviceset="SMD-RGB-LED-4P" device="'WS2812B'"/>
<part name="SUPPLY18" library="supply2" deviceset="GND" device=""/>
<part name="D7" library="Seeed-OPL-2017-01-Diode-james" deviceset="SMD-RGB-LED-4P" device="'WS2812B'"/>
<part name="SUPPLY19" library="supply2" deviceset="GND" device=""/>
<part name="D8" library="Seeed-OPL-2017-01-Diode-james" deviceset="SMD-RGB-LED-4P" device="'WS2812B'"/>
<part name="SUPPLY20" library="supply2" deviceset="GND" device=""/>
<part name="D9" library="Seeed-OPL-2017-01-Diode-james" deviceset="SMD-RGB-LED-4P" device="'WS2812B'"/>
<part name="SUPPLY21" library="supply2" deviceset="GND" device=""/>
<part name="D10" library="Seeed-OPL-2017-01-Diode-james" deviceset="SMD-RGB-LED-4P" device="'WS2812B'"/>
<part name="SUPPLY22" library="supply2" deviceset="GND" device=""/>
<part name="D11" library="Seeed-OPL-2017-01-Diode-james" deviceset="SMD-RGB-LED-4P" device="'WS2812B'"/>
<part name="SUPPLY23" library="supply2" deviceset="GND" device=""/>
<part name="D12" library="Seeed-OPL-2017-01-Diode-james" deviceset="SMD-RGB-LED-4P" device="'WS2812B'"/>
<part name="SUPPLY24" library="supply2" deviceset="GND" device=""/>
<part name="D13" library="Seeed-OPL-2017-01-Diode-james" deviceset="SMD-RGB-LED-4P" device="'WS2812B'"/>
<part name="SUPPLY25" library="supply2" deviceset="GND" device=""/>
<part name="D14" library="Seeed-OPL-2017-01-Diode-james" deviceset="SMD-RGB-LED-4P" device="'WS2812B'"/>
<part name="SUPPLY26" library="supply2" deviceset="GND" device=""/>
<part name="D15" library="Seeed-OPL-2017-01-Diode-james" deviceset="SMD-RGB-LED-4P" device="'WS2812B'"/>
<part name="SUPPLY27" library="supply2" deviceset="GND" device=""/>
<part name="D16" library="Seeed-OPL-2017-01-Diode-james" deviceset="SMD-RGB-LED-4P" device="'WS2812B'"/>
<part name="SUPPLY28" library="supply2" deviceset="GND" device=""/>
<part name="D17" library="Seeed-OPL-2017-01-Diode-james" deviceset="SMD-RGB-LED-4P" device="'WS2812B'"/>
<part name="SUPPLY29" library="supply2" deviceset="GND" device=""/>
<part name="U1" library="Bluetooth%20HC-05" deviceset="HC_05" device=""/>
<part name="U3" library="Seeed-IC-2016" deviceset="PMIC-CJT1117-3.3(SOT223)" device="" value="CJT1117-3.3-SOT223"/>
<part name="SUPPLY38" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY39" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY40" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY41" library="supply2" deviceset="GND" device=""/>
<part name="Q1" library="Seeed-Transistor-2016" deviceset="SMD-MOSFET-N-CH-20V-2.1A-CJ2302(SOT-23)" device="" value="CJ2302"/>
<part name="Q2" library="Seeed-Transistor-2016" deviceset="SMD-MOSFET-N-CH-20V-2.1A-CJ2302(SOT-23)" device="" value="CJ2302"/>
<part name="R4" library="Seeed-Resistor-2016" deviceset="SMD-RES-4.7K-5%-1/10W(0603)" device="" value="4.7K"/>
<part name="R5" library="Seeed-Resistor-2016" deviceset="SMD-RES-4.7K-5%-1/10W(0603)" device="" value="4.7K"/>
<part name="SJ1" library="SparkFun-Retired" deviceset="SOLDERJUMPER" device="NO"/>
<part name="SJ2" library="SparkFun-Retired" deviceset="SOLDERJUMPER" device="NO"/>
<part name="R6" library="Seeed-Resistor-2016" deviceset="SMD-RES-4.7K-5%-1/10W(0603)" device="" value="4.7K"/>
<part name="R7" library="Seeed-Resistor-2016" deviceset="SMD-RES-4.7K-5%-1/10W(0603)" device="" value="4.7K"/>
<part name="C28" library="Seeed-Capacitor-2016" deviceset="TANTALUM-SMD-4.7UF-16V(AVX-A)" device="" value="4.7uf"/>
<part name="C29" library="Seeed-Capacitor-2016" deviceset="CERAMIC-100NF-50V-10%-X7R(0603)" device="" value="100nf"/>
<part name="SUPPLY42" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY43" library="supply2" deviceset="GND" device=""/>
<part name="C30" library="Seeed-Capacitor-2016" deviceset="CERAMIC-100NF-50V-10%-X7R(0603)" device="" value="100nf"/>
<part name="SUPPLY44" library="supply2" deviceset="GND" device=""/>
<part name="R1" library="Seeed-Resistor-2016" deviceset="SMD-RES-4.7K-5%-1/10W(0603)" device="" value="4.7K"/>
<part name="C31" library="Seeed-Capacitor-2016" deviceset="CERAMIC-100NF-50V-10%-X7R(0603)" device="" value="100nf"/>
<part name="SUPPLY45" library="supply2" deviceset="GND" device=""/>
<part name="D18" library="Seeed-OPL-2017-01-Diode-james" deviceset="SMD-RGB-LED-4P" device="'WS2812B'"/>
<part name="SUPPLY16" library="supply2" deviceset="GND" device=""/>
<part name="D19" library="Seeed-OPL-2017-01-Diode-james" deviceset="SMD-RGB-LED-4P" device="'WS2812B'"/>
<part name="SUPPLY30" library="supply2" deviceset="GND" device=""/>
<part name="D20" library="Seeed-OPL-2017-01-Diode-james" deviceset="SMD-RGB-LED-4P" device="'WS2812B'"/>
<part name="SUPPLY31" library="supply2" deviceset="GND" device=""/>
<part name="D21" library="Seeed-OPL-2017-01-Diode-james" deviceset="SMD-RGB-LED-4P" device="'WS2812B'"/>
<part name="SUPPLY32" library="supply2" deviceset="GND" device=""/>
<part name="D22" library="Seeed-OPL-2017-01-Diode-james" deviceset="SMD-RGB-LED-4P" device="'WS2812B'"/>
<part name="SUPPLY33" library="supply2" deviceset="GND" device=""/>
<part name="D23" library="Seeed-OPL-2017-01-Diode-james" deviceset="SMD-RGB-LED-4P" device="'WS2812B'"/>
<part name="SUPPLY34" library="supply2" deviceset="GND" device=""/>
<part name="D24" library="Seeed-OPL-2017-01-Diode-james" deviceset="SMD-RGB-LED-4P" device="'WS2812B'"/>
<part name="SUPPLY35" library="supply2" deviceset="GND" device=""/>
<part name="D25" library="Seeed-OPL-2017-01-Diode-james" deviceset="SMD-RGB-LED-4P" device="'WS2812B'"/>
<part name="SUPPLY36" library="supply2" deviceset="GND" device=""/>
<part name="C16" library="Seeed-Capacitor-2016" deviceset="CERAMIC-100NF-50V-10%-X7R(0805)" device="" value="100nf"/>
<part name="C17" library="Seeed-Capacitor-2016" deviceset="CERAMIC-100NF-50V-10%-X7R(0805)" device="" value="100nf"/>
<part name="C18" library="Seeed-Capacitor-2016" deviceset="CERAMIC-100NF-50V-10%-X7R(0805)" device="" value="100nf"/>
<part name="C19" library="Seeed-Capacitor-2016" deviceset="CERAMIC-100NF-50V-10%-X7R(0805)" device="" value="100nf"/>
<part name="C20" library="Seeed-Capacitor-2016" deviceset="CERAMIC-100NF-50V-10%-X7R(0805)" device="" value="100nf"/>
<part name="C21" library="Seeed-Capacitor-2016" deviceset="CERAMIC-100NF-50V-10%-X7R(0805)" device="" value="100nf"/>
<part name="C22" library="Seeed-Capacitor-2016" deviceset="CERAMIC-100NF-50V-10%-X7R(0805)" device="" value="100nf"/>
<part name="C23" library="Seeed-Capacitor-2016" deviceset="CERAMIC-100NF-50V-10%-X7R(0805)" device="" value="100nf"/>
<part name="C32" library="Seeed-Capacitor-2016" deviceset="CERAMIC-100NF-50V-10%-X7R(0603)" device="" value="100nf"/>
<part name="SUPPLY37" library="supply2" deviceset="GND" device=""/>
<part name="U4" library="Seeed-Sensor-2016" deviceset="PHOTORESISTOR-5528-10K(2P-5.0X4.2MM)" device="" value="PHOTORESISTOR-5528-10K"/>
<part name="R2" library="Seeed-Resistor-2016" deviceset="SMD-RES-4.7K-5%-1/10W(0603)" device="" value="4.7K"/>
<part name="SUPPLY46" library="supply2" deviceset="GND" device=""/>
<part name="C33" library="Seeed-Capacitor-2016" deviceset="CERAMIC-100NF-50V-10%-X7R(0603)" device="" value="100nf"/>
<part name="SUPPLY47" library="supply2" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="D1" gate="G$1" x="7.62" y="-54.61"/>
<instance part="SUPPLY8" gate="GND" x="21.59" y="-59.69"/>
<instance part="D2" gate="G$1" x="-38.1" y="85.09"/>
<instance part="F1" gate="G$1" x="-62.23" y="85.09"/>
<instance part="USB1" gate="G$1" x="-110.49" y="82.55"/>
<instance part="U2" gate="G$1" x="-7.62" y="33.02"/>
<instance part="J2" gate="G$1" x="73.66" y="35.56"/>
<instance part="SUPPLY1" gate="GND" x="-125.73" y="76.2"/>
<instance part="SUPPLY2" gate="GND" x="-96.52" y="72.39"/>
<instance part="SUPPLY3" gate="GND" x="-35.56" y="10.16" rot="R270"/>
<instance part="SUPPLY4" gate="GND" x="-33.02" y="0" rot="R270"/>
<instance part="SW1" gate="G$1" x="48.26" y="7.62"/>
<instance part="SUPPLY5" gate="GND" x="48.26" y="-5.08"/>
<instance part="C1" gate="G$1" x="64.77" y="15.24"/>
<instance part="Y1" gate="G$1" x="21.59" y="38.1" rot="R90"/>
<instance part="SUPPLY9" gate="GND" x="31.75" y="38.1" rot="R90"/>
<instance part="SUPPLY10" gate="GND" x="-39.37" y="55.88" rot="R180"/>
<instance part="C2" gate="G$1" x="-43.18" y="45.72" rot="R270"/>
<instance part="SUPPLY11" gate="GND" x="-43.18" y="39.37"/>
<instance part="J4" gate="G$1" x="73.66" y="88.9"/>
<instance part="SUPPLY6" gate="GND" x="81.28" y="78.74"/>
<instance part="CT1" gate="G$1" x="-26.67" y="81.28" rot="R270"/>
<instance part="SUPPLY7" gate="GND" x="-26.67" y="74.93"/>
<instance part="C3" gate="G$1" x="-16.51" y="81.28" rot="R270"/>
<instance part="SUPPLY12" gate="GND" x="-16.51" y="74.93"/>
<instance part="C4" gate="G$1" x="-133.35" y="24.13" rot="R90"/>
<instance part="C5" gate="G$1" x="-129.54" y="24.13" rot="R90"/>
<instance part="C6" gate="G$1" x="-125.73" y="24.13" rot="R90"/>
<instance part="C7" gate="G$1" x="-121.92" y="24.13" rot="R90"/>
<instance part="C8" gate="G$1" x="-118.11" y="24.13" rot="R90"/>
<instance part="C9" gate="G$1" x="-114.3" y="24.13" rot="R90"/>
<instance part="C10" gate="G$1" x="-110.49" y="24.13" rot="R90"/>
<instance part="C11" gate="G$1" x="-106.68" y="24.13" rot="R90"/>
<instance part="C12" gate="G$1" x="-148.59" y="24.13" rot="R90"/>
<instance part="C13" gate="G$1" x="-144.78" y="24.13" rot="R90"/>
<instance part="C14" gate="G$1" x="-140.97" y="24.13" rot="R90"/>
<instance part="C15" gate="G$1" x="-137.16" y="24.13" rot="R90"/>
<instance part="C24" gate="G$1" x="-152.4" y="24.13" rot="R90"/>
<instance part="C25" gate="G$1" x="-102.87" y="24.13" rot="R90"/>
<instance part="C26" gate="G$1" x="-156.21" y="24.13" rot="R90"/>
<instance part="C27" gate="G$1" x="-99.06" y="24.13" rot="R90"/>
<instance part="SUPPLY15" gate="GND" x="-129.54" y="12.7"/>
<instance part="D3" gate="G$1" x="-26.67" y="-54.61"/>
<instance part="SUPPLY13" gate="GND" x="-12.7" y="-59.69"/>
<instance part="D4" gate="G$1" x="-60.96" y="-54.61"/>
<instance part="SUPPLY14" gate="GND" x="-46.99" y="-59.69"/>
<instance part="D5" gate="G$1" x="-93.98" y="-54.61"/>
<instance part="SUPPLY17" gate="GND" x="-80.01" y="-59.69"/>
<instance part="D6" gate="G$1" x="-127" y="-54.61"/>
<instance part="SUPPLY18" gate="GND" x="-113.03" y="-59.69"/>
<instance part="D7" gate="G$1" x="-160.02" y="-54.61"/>
<instance part="SUPPLY19" gate="GND" x="-146.05" y="-59.69"/>
<instance part="D8" gate="G$1" x="-193.04" y="-54.61"/>
<instance part="SUPPLY20" gate="GND" x="-179.07" y="-59.69"/>
<instance part="D9" gate="G$1" x="-227.33" y="-54.61"/>
<instance part="SUPPLY21" gate="GND" x="-213.36" y="-59.69"/>
<instance part="D10" gate="G$1" x="7.62" y="-80.01"/>
<instance part="SUPPLY22" gate="GND" x="21.59" y="-85.09"/>
<instance part="D11" gate="G$1" x="-25.4" y="-80.01"/>
<instance part="SUPPLY23" gate="GND" x="-11.43" y="-85.09"/>
<instance part="D12" gate="G$1" x="-58.42" y="-80.01"/>
<instance part="SUPPLY24" gate="GND" x="-44.45" y="-85.09"/>
<instance part="D13" gate="G$1" x="-91.44" y="-80.01"/>
<instance part="SUPPLY25" gate="GND" x="-77.47" y="-85.09"/>
<instance part="D14" gate="G$1" x="-127" y="-81.28"/>
<instance part="SUPPLY26" gate="GND" x="-113.03" y="-86.36"/>
<instance part="D15" gate="G$1" x="-161.29" y="-81.28"/>
<instance part="SUPPLY27" gate="GND" x="-147.32" y="-86.36"/>
<instance part="D16" gate="G$1" x="-195.58" y="-81.28"/>
<instance part="SUPPLY28" gate="GND" x="-181.61" y="-86.36"/>
<instance part="D17" gate="G$1" x="-228.6" y="-81.28"/>
<instance part="SUPPLY29" gate="GND" x="-214.63" y="-86.36"/>
<instance part="U1" gate="G$1" x="116.84" y="-81.28"/>
<instance part="U3" gate="G$1" x="63.5" y="-40.64"/>
<instance part="SUPPLY38" gate="GND" x="63.5" y="-53.34"/>
<instance part="SUPPLY39" gate="GND" x="92.71" y="-99.06"/>
<instance part="SUPPLY40" gate="GND" x="143.51" y="-99.06"/>
<instance part="SUPPLY41" gate="GND" x="127" y="-106.68"/>
<instance part="Q1" gate="G$1" x="113.03" y="-36.83" rot="R270"/>
<instance part="Q2" gate="G$1" x="157.48" y="-39.37" rot="R270"/>
<instance part="R4" gate="G$1" x="107.95" y="-31.75" rot="R90"/>
<instance part="R5" gate="G$1" x="152.4" y="-34.29" rot="R90"/>
<instance part="SJ1" gate="1" x="125.73" y="-36.83"/>
<instance part="SJ2" gate="1" x="170.18" y="-39.37"/>
<instance part="R6" gate="G$1" x="119.38" y="-31.75" rot="R90"/>
<instance part="R7" gate="G$1" x="163.83" y="-34.29" rot="R90"/>
<instance part="C28" gate="G$1" x="76.2" y="-44.45" rot="R270"/>
<instance part="C29" gate="G$1" x="82.55" y="-44.45" rot="R270"/>
<instance part="SUPPLY42" gate="GND" x="76.2" y="-50.8"/>
<instance part="SUPPLY43" gate="GND" x="82.55" y="-50.8"/>
<instance part="C30" gate="G$1" x="52.07" y="-44.45" rot="R270"/>
<instance part="SUPPLY44" gate="GND" x="52.07" y="-50.8"/>
<instance part="R1" gate="G$1" x="48.26" y="19.05" rot="R90"/>
<instance part="C31" gate="G$1" x="-34.29" y="21.59" rot="R270"/>
<instance part="SUPPLY45" gate="GND" x="-34.29" y="15.24"/>
<instance part="D18" gate="G$1" x="-58.42" y="-29.21"/>
<instance part="SUPPLY16" gate="GND" x="-44.45" y="-34.29"/>
<instance part="D19" gate="G$1" x="-92.71" y="-29.21"/>
<instance part="SUPPLY30" gate="GND" x="-78.74" y="-34.29"/>
<instance part="D20" gate="G$1" x="-127" y="-29.21"/>
<instance part="SUPPLY31" gate="GND" x="-113.03" y="-34.29"/>
<instance part="D21" gate="G$1" x="-160.02" y="-29.21"/>
<instance part="SUPPLY32" gate="GND" x="-146.05" y="-34.29"/>
<instance part="D22" gate="G$1" x="-193.04" y="-29.21"/>
<instance part="SUPPLY33" gate="GND" x="-179.07" y="-34.29"/>
<instance part="D23" gate="G$1" x="-226.06" y="-29.21"/>
<instance part="SUPPLY34" gate="GND" x="-212.09" y="-34.29"/>
<instance part="D24" gate="G$1" x="7.62" y="-29.21"/>
<instance part="SUPPLY35" gate="GND" x="21.59" y="-34.29"/>
<instance part="D25" gate="G$1" x="-25.4" y="-29.21"/>
<instance part="SUPPLY36" gate="GND" x="-11.43" y="-34.29"/>
<instance part="C16" gate="G$1" x="-95.25" y="24.13" rot="R90"/>
<instance part="C17" gate="G$1" x="-90.17" y="24.13" rot="R90"/>
<instance part="C18" gate="G$1" x="-85.09" y="24.13" rot="R90"/>
<instance part="C19" gate="G$1" x="-80.01" y="24.13" rot="R90"/>
<instance part="C20" gate="G$1" x="-74.93" y="24.13" rot="R90"/>
<instance part="C21" gate="G$1" x="-69.85" y="24.13" rot="R90"/>
<instance part="C22" gate="G$1" x="-66.04" y="24.13" rot="R90"/>
<instance part="C23" gate="G$1" x="-62.23" y="24.13" rot="R90"/>
<instance part="C32" gate="G$1" x="-38.1" y="21.59" rot="R270"/>
<instance part="SUPPLY37" gate="GND" x="-38.1" y="15.24"/>
<instance part="U4" gate="G$1" x="104.14" y="2.54" rot="R90"/>
<instance part="R2" gate="G$1" x="104.14" y="15.24" rot="R90"/>
<instance part="SUPPLY46" gate="GND" x="104.14" y="-6.35"/>
<instance part="C33" gate="G$1" x="109.22" y="2.54" rot="R90"/>
<instance part="SUPPLY47" gate="GND" x="109.22" y="-6.35"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="VSS"/>
<pinref part="SUPPLY8" gate="GND" pin="GND"/>
<wire x1="19.05" y1="-55.88" x2="21.59" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="21.59" y1="-55.88" x2="21.59" y2="-57.15" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="1"/>
<wire x1="78.74" y1="30.48" x2="81.28" y2="30.48" width="0.1524" layer="91"/>
<label x="81.28" y="30.48" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="2"/>
<wire x1="78.74" y1="33.02" x2="81.28" y2="33.02" width="0.1524" layer="91"/>
<label x="81.28" y="33.02" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="USB1" gate="G$1" pin="GND@1"/>
<wire x1="-123.19" y1="86.36" x2="-125.73" y2="86.36" width="0.1524" layer="91"/>
<pinref part="USB1" gate="G$1" pin="GND@4"/>
<wire x1="-123.19" y1="78.74" x2="-125.73" y2="78.74" width="0.1524" layer="91"/>
<wire x1="-125.73" y1="86.36" x2="-125.73" y2="83.82" width="0.1524" layer="91"/>
<pinref part="SUPPLY1" gate="GND" pin="GND"/>
<junction x="-125.73" y="78.74"/>
<pinref part="USB1" gate="G$1" pin="GND@3"/>
<wire x1="-125.73" y1="83.82" x2="-125.73" y2="81.28" width="0.1524" layer="91"/>
<wire x1="-125.73" y1="81.28" x2="-125.73" y2="78.74" width="0.1524" layer="91"/>
<wire x1="-123.19" y1="81.28" x2="-125.73" y2="81.28" width="0.1524" layer="91"/>
<junction x="-125.73" y="81.28"/>
<pinref part="USB1" gate="G$1" pin="GND@2"/>
<wire x1="-123.19" y1="83.82" x2="-125.73" y2="83.82" width="0.1524" layer="91"/>
<junction x="-125.73" y="83.82"/>
</segment>
<segment>
<pinref part="USB1" gate="G$1" pin="GND@5"/>
<wire x1="-96.52" y1="77.47" x2="-96.52" y2="74.93" width="0.1524" layer="91"/>
<pinref part="SUPPLY2" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="GND@1"/>
<pinref part="U2" gate="G$1" pin="GND@2"/>
<wire x1="-30.48" y1="10.16" x2="-30.48" y2="12.7" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="10.16" x2="-33.02" y2="10.16" width="0.1524" layer="91"/>
<junction x="-30.48" y="10.16"/>
<pinref part="SUPPLY3" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="PAD"/>
<pinref part="SUPPLY4" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="SW1" gate="G$1" pin="B0"/>
<wire x1="39.37" y1="5.08" x2="38.1" y2="5.08" width="0.1524" layer="91"/>
<wire x1="38.1" y1="5.08" x2="38.1" y2="0" width="0.1524" layer="91"/>
<wire x1="38.1" y1="0" x2="48.26" y2="0" width="0.1524" layer="91"/>
<wire x1="48.26" y1="0" x2="58.42" y2="0" width="0.1524" layer="91"/>
<wire x1="58.42" y1="0" x2="58.42" y2="5.08" width="0.1524" layer="91"/>
<pinref part="SW1" gate="G$1" pin="B1"/>
<wire x1="58.42" y1="5.08" x2="57.15" y2="5.08" width="0.1524" layer="91"/>
<wire x1="48.26" y1="0" x2="48.26" y2="-2.54" width="0.1524" layer="91"/>
<junction x="48.26" y="0"/>
<pinref part="SUPPLY5" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="Y1" gate="G$1" pin="2"/>
<pinref part="SUPPLY9" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="GND"/>
<pinref part="SUPPLY10" gate="GND" pin="GND"/>
<wire x1="-39.37" y1="53.34" x2="-30.48" y2="53.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="2"/>
<pinref part="SUPPLY11" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="1"/>
<wire x1="78.74" y1="83.82" x2="81.28" y2="83.82" width="0.1524" layer="91"/>
<wire x1="81.28" y1="83.82" x2="81.28" y2="81.28" width="0.1524" layer="91"/>
<pinref part="SUPPLY6" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="-"/>
<pinref part="SUPPLY12" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="-148.59" y1="20.32" x2="-148.59" y2="19.05" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="-148.59" y1="19.05" x2="-144.78" y2="19.05" width="0.1524" layer="91"/>
<wire x1="-144.78" y1="19.05" x2="-140.97" y2="19.05" width="0.1524" layer="91"/>
<wire x1="-140.97" y1="19.05" x2="-137.16" y2="19.05" width="0.1524" layer="91"/>
<wire x1="-137.16" y1="19.05" x2="-133.35" y2="19.05" width="0.1524" layer="91"/>
<wire x1="-125.73" y1="19.05" x2="-121.92" y2="19.05" width="0.1524" layer="91"/>
<wire x1="-121.92" y1="19.05" x2="-118.11" y2="19.05" width="0.1524" layer="91"/>
<wire x1="-118.11" y1="19.05" x2="-114.3" y2="19.05" width="0.1524" layer="91"/>
<wire x1="-114.3" y1="19.05" x2="-110.49" y2="19.05" width="0.1524" layer="91"/>
<wire x1="-110.49" y1="19.05" x2="-106.68" y2="19.05" width="0.1524" layer="91"/>
<wire x1="-106.68" y1="19.05" x2="-106.68" y2="20.32" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="-144.78" y1="20.32" x2="-144.78" y2="19.05" width="0.1524" layer="91"/>
<junction x="-144.78" y="19.05"/>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="-140.97" y1="20.32" x2="-140.97" y2="19.05" width="0.1524" layer="91"/>
<junction x="-140.97" y="19.05"/>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="-137.16" y1="20.32" x2="-137.16" y2="19.05" width="0.1524" layer="91"/>
<junction x="-137.16" y="19.05"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="-133.35" y1="20.32" x2="-133.35" y2="19.05" width="0.1524" layer="91"/>
<junction x="-133.35" y="19.05"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="-129.54" y1="20.32" x2="-129.54" y2="19.05" width="0.1524" layer="91"/>
<junction x="-129.54" y="19.05"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="-129.54" y1="19.05" x2="-129.54" y2="15.24" width="0.1524" layer="91"/>
<wire x1="-125.73" y1="20.32" x2="-125.73" y2="19.05" width="0.1524" layer="91"/>
<junction x="-125.73" y="19.05"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="-121.92" y1="20.32" x2="-121.92" y2="19.05" width="0.1524" layer="91"/>
<junction x="-121.92" y="19.05"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="-118.11" y1="20.32" x2="-118.11" y2="19.05" width="0.1524" layer="91"/>
<junction x="-118.11" y="19.05"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="-114.3" y1="20.32" x2="-114.3" y2="19.05" width="0.1524" layer="91"/>
<junction x="-114.3" y="19.05"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="-110.49" y1="20.32" x2="-110.49" y2="19.05" width="0.1524" layer="91"/>
<junction x="-110.49" y="19.05"/>
<pinref part="SUPPLY15" gate="GND" pin="GND"/>
<pinref part="C26" gate="G$1" pin="1"/>
<wire x1="-156.21" y1="20.32" x2="-156.21" y2="19.05" width="0.1524" layer="91"/>
<wire x1="-156.21" y1="19.05" x2="-152.4" y2="19.05" width="0.1524" layer="91"/>
<junction x="-148.59" y="19.05"/>
<pinref part="C24" gate="G$1" pin="1"/>
<wire x1="-152.4" y1="19.05" x2="-148.59" y2="19.05" width="0.1524" layer="91"/>
<wire x1="-152.4" y1="20.32" x2="-152.4" y2="19.05" width="0.1524" layer="91"/>
<junction x="-152.4" y="19.05"/>
<pinref part="C27" gate="G$1" pin="1"/>
<wire x1="-106.68" y1="19.05" x2="-102.87" y2="19.05" width="0.1524" layer="91"/>
<wire x1="-102.87" y1="19.05" x2="-99.06" y2="19.05" width="0.1524" layer="91"/>
<wire x1="-99.06" y1="19.05" x2="-99.06" y2="20.32" width="0.1524" layer="91"/>
<junction x="-106.68" y="19.05"/>
<pinref part="C25" gate="G$1" pin="1"/>
<wire x1="-102.87" y1="20.32" x2="-102.87" y2="19.05" width="0.1524" layer="91"/>
<junction x="-102.87" y="19.05"/>
<wire x1="-133.35" y1="19.05" x2="-129.54" y2="19.05" width="0.1524" layer="91"/>
<wire x1="-129.54" y1="19.05" x2="-125.73" y2="19.05" width="0.1524" layer="91"/>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="-99.06" y1="19.05" x2="-95.25" y2="19.05" width="0.1524" layer="91"/>
<wire x1="-95.25" y1="19.05" x2="-90.17" y2="19.05" width="0.1524" layer="91"/>
<wire x1="-90.17" y1="19.05" x2="-85.09" y2="19.05" width="0.1524" layer="91"/>
<wire x1="-85.09" y1="19.05" x2="-80.01" y2="19.05" width="0.1524" layer="91"/>
<wire x1="-80.01" y1="19.05" x2="-74.93" y2="19.05" width="0.1524" layer="91"/>
<wire x1="-74.93" y1="19.05" x2="-69.85" y2="19.05" width="0.1524" layer="91"/>
<wire x1="-69.85" y1="19.05" x2="-66.04" y2="19.05" width="0.1524" layer="91"/>
<wire x1="-66.04" y1="19.05" x2="-62.23" y2="19.05" width="0.1524" layer="91"/>
<wire x1="-62.23" y1="19.05" x2="-62.23" y2="20.32" width="0.1524" layer="91"/>
<junction x="-99.06" y="19.05"/>
<pinref part="C22" gate="G$1" pin="1"/>
<wire x1="-66.04" y1="20.32" x2="-66.04" y2="19.05" width="0.1524" layer="91"/>
<junction x="-66.04" y="19.05"/>
<pinref part="C21" gate="G$1" pin="1"/>
<wire x1="-69.85" y1="20.32" x2="-69.85" y2="19.05" width="0.1524" layer="91"/>
<junction x="-69.85" y="19.05"/>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="-74.93" y1="20.32" x2="-74.93" y2="19.05" width="0.1524" layer="91"/>
<junction x="-74.93" y="19.05"/>
<pinref part="C19" gate="G$1" pin="1"/>
<wire x1="-80.01" y1="20.32" x2="-80.01" y2="19.05" width="0.1524" layer="91"/>
<junction x="-80.01" y="19.05"/>
<pinref part="C18" gate="G$1" pin="1"/>
<wire x1="-85.09" y1="20.32" x2="-85.09" y2="19.05" width="0.1524" layer="91"/>
<junction x="-85.09" y="19.05"/>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="-90.17" y1="20.32" x2="-90.17" y2="19.05" width="0.1524" layer="91"/>
<junction x="-90.17" y="19.05"/>
<pinref part="C16" gate="G$1" pin="1"/>
<wire x1="-95.25" y1="20.32" x2="-95.25" y2="19.05" width="0.1524" layer="91"/>
<junction x="-95.25" y="19.05"/>
</segment>
<segment>
<pinref part="CT1" gate="G$1" pin="-"/>
<pinref part="SUPPLY7" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="D3" gate="G$1" pin="VSS"/>
<pinref part="SUPPLY13" gate="GND" pin="GND"/>
<wire x1="-15.24" y1="-55.88" x2="-12.7" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="-55.88" x2="-12.7" y2="-57.15" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D4" gate="G$1" pin="VSS"/>
<pinref part="SUPPLY14" gate="GND" pin="GND"/>
<wire x1="-49.53" y1="-55.88" x2="-46.99" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="-46.99" y1="-55.88" x2="-46.99" y2="-57.15" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D5" gate="G$1" pin="VSS"/>
<pinref part="SUPPLY17" gate="GND" pin="GND"/>
<wire x1="-82.55" y1="-55.88" x2="-80.01" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="-80.01" y1="-55.88" x2="-80.01" y2="-57.15" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D6" gate="G$1" pin="VSS"/>
<pinref part="SUPPLY18" gate="GND" pin="GND"/>
<wire x1="-115.57" y1="-55.88" x2="-113.03" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="-113.03" y1="-55.88" x2="-113.03" y2="-57.15" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D7" gate="G$1" pin="VSS"/>
<pinref part="SUPPLY19" gate="GND" pin="GND"/>
<wire x1="-148.59" y1="-55.88" x2="-146.05" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="-146.05" y1="-55.88" x2="-146.05" y2="-57.15" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D8" gate="G$1" pin="VSS"/>
<pinref part="SUPPLY20" gate="GND" pin="GND"/>
<wire x1="-181.61" y1="-55.88" x2="-179.07" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="-179.07" y1="-55.88" x2="-179.07" y2="-57.15" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D9" gate="G$1" pin="VSS"/>
<pinref part="SUPPLY21" gate="GND" pin="GND"/>
<wire x1="-215.9" y1="-55.88" x2="-213.36" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="-213.36" y1="-55.88" x2="-213.36" y2="-57.15" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D10" gate="G$1" pin="VSS"/>
<pinref part="SUPPLY22" gate="GND" pin="GND"/>
<wire x1="19.05" y1="-81.28" x2="21.59" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="21.59" y1="-81.28" x2="21.59" y2="-82.55" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D11" gate="G$1" pin="VSS"/>
<pinref part="SUPPLY23" gate="GND" pin="GND"/>
<wire x1="-13.97" y1="-81.28" x2="-11.43" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="-11.43" y1="-81.28" x2="-11.43" y2="-82.55" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D12" gate="G$1" pin="VSS"/>
<pinref part="SUPPLY24" gate="GND" pin="GND"/>
<wire x1="-46.99" y1="-81.28" x2="-44.45" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="-44.45" y1="-81.28" x2="-44.45" y2="-82.55" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D13" gate="G$1" pin="VSS"/>
<pinref part="SUPPLY25" gate="GND" pin="GND"/>
<wire x1="-80.01" y1="-81.28" x2="-77.47" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="-77.47" y1="-81.28" x2="-77.47" y2="-82.55" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D14" gate="G$1" pin="VSS"/>
<pinref part="SUPPLY26" gate="GND" pin="GND"/>
<wire x1="-115.57" y1="-82.55" x2="-113.03" y2="-82.55" width="0.1524" layer="91"/>
<wire x1="-113.03" y1="-82.55" x2="-113.03" y2="-83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D15" gate="G$1" pin="VSS"/>
<pinref part="SUPPLY27" gate="GND" pin="GND"/>
<wire x1="-149.86" y1="-82.55" x2="-147.32" y2="-82.55" width="0.1524" layer="91"/>
<wire x1="-147.32" y1="-82.55" x2="-147.32" y2="-83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D16" gate="G$1" pin="VSS"/>
<pinref part="SUPPLY28" gate="GND" pin="GND"/>
<wire x1="-184.15" y1="-82.55" x2="-181.61" y2="-82.55" width="0.1524" layer="91"/>
<wire x1="-181.61" y1="-82.55" x2="-181.61" y2="-83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D17" gate="G$1" pin="VSS"/>
<pinref part="SUPPLY29" gate="GND" pin="GND"/>
<wire x1="-217.17" y1="-82.55" x2="-214.63" y2="-82.55" width="0.1524" layer="91"/>
<wire x1="-214.63" y1="-82.55" x2="-214.63" y2="-83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="GND"/>
<pinref part="SUPPLY38" gate="GND" pin="GND"/>
<wire x1="63.5" y1="-50.8" x2="63.5" y2="-48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="GND"/>
<wire x1="93.98" y1="-96.52" x2="92.71" y2="-96.52" width="0.1524" layer="91"/>
<pinref part="SUPPLY39" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="GND2"/>
<pinref part="SUPPLY40" gate="GND" pin="GND"/>
<wire x1="143.51" y1="-96.52" x2="142.24" y2="-96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="GND1"/>
<pinref part="SUPPLY41" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C28" gate="G$1" pin="-"/>
<pinref part="SUPPLY42" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C29" gate="G$1" pin="2"/>
<pinref part="SUPPLY43" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C30" gate="G$1" pin="2"/>
<pinref part="SUPPLY44" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C31" gate="G$1" pin="2"/>
<pinref part="SUPPLY45" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="D18" gate="G$1" pin="VSS"/>
<pinref part="SUPPLY16" gate="GND" pin="GND"/>
<wire x1="-46.99" y1="-30.48" x2="-44.45" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="-44.45" y1="-30.48" x2="-44.45" y2="-31.75" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D19" gate="G$1" pin="VSS"/>
<pinref part="SUPPLY30" gate="GND" pin="GND"/>
<wire x1="-81.28" y1="-30.48" x2="-78.74" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="-78.74" y1="-30.48" x2="-78.74" y2="-31.75" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D20" gate="G$1" pin="VSS"/>
<pinref part="SUPPLY31" gate="GND" pin="GND"/>
<wire x1="-115.57" y1="-30.48" x2="-113.03" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="-113.03" y1="-30.48" x2="-113.03" y2="-31.75" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D21" gate="G$1" pin="VSS"/>
<pinref part="SUPPLY32" gate="GND" pin="GND"/>
<wire x1="-148.59" y1="-30.48" x2="-146.05" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="-146.05" y1="-30.48" x2="-146.05" y2="-31.75" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D22" gate="G$1" pin="VSS"/>
<pinref part="SUPPLY33" gate="GND" pin="GND"/>
<wire x1="-181.61" y1="-30.48" x2="-179.07" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="-179.07" y1="-30.48" x2="-179.07" y2="-31.75" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D23" gate="G$1" pin="VSS"/>
<pinref part="SUPPLY34" gate="GND" pin="GND"/>
<wire x1="-214.63" y1="-30.48" x2="-212.09" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="-212.09" y1="-30.48" x2="-212.09" y2="-31.75" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D24" gate="G$1" pin="VSS"/>
<pinref part="SUPPLY35" gate="GND" pin="GND"/>
<wire x1="19.05" y1="-30.48" x2="21.59" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="21.59" y1="-30.48" x2="21.59" y2="-31.75" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D25" gate="G$1" pin="VSS"/>
<pinref part="SUPPLY36" gate="GND" pin="GND"/>
<wire x1="-13.97" y1="-30.48" x2="-11.43" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="-11.43" y1="-30.48" x2="-11.43" y2="-31.75" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C32" gate="G$1" pin="2"/>
<pinref part="SUPPLY37" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="P$1"/>
<pinref part="SUPPLY46" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C33" gate="G$1" pin="1"/>
<pinref part="SUPPLY47" gate="GND" pin="GND"/>
<wire x1="109.22" y1="-3.81" x2="109.22" y2="-1.27" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DTR" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="6"/>
<wire x1="78.74" y1="43.18" x2="81.28" y2="43.18" width="0.1524" layer="91"/>
<label x="81.28" y="43.18" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="68.58" y1="15.24" x2="71.12" y2="15.24" width="0.1524" layer="91"/>
<label x="71.12" y="15.24" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="TXO" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="5"/>
<wire x1="78.74" y1="40.64" x2="81.28" y2="40.64" width="0.1524" layer="91"/>
<label x="81.28" y="40.64" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="PD1(TXD/PCINT17)"/>
<wire x1="17.78" y1="7.62" x2="20.32" y2="7.62" width="0.1524" layer="91"/>
<label x="20.32" y="7.62" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="SJ2" gate="1" pin="2"/>
<wire x1="175.26" y1="-39.37" x2="176.53" y2="-39.37" width="0.1524" layer="91"/>
<label x="176.53" y="-39.37" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="RXI" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="4"/>
<wire x1="78.74" y1="38.1" x2="81.28" y2="38.1" width="0.1524" layer="91"/>
<label x="81.28" y="38.1" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="PD0(RXD/PCINT16)"/>
<wire x1="17.78" y1="10.16" x2="20.32" y2="10.16" width="0.1524" layer="91"/>
<label x="20.32" y="10.16" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="SJ1" gate="1" pin="2"/>
<wire x1="130.81" y1="-36.83" x2="132.08" y2="-36.83" width="0.1524" layer="91"/>
<label x="132.08" y="-36.83" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="3"/>
<wire x1="78.74" y1="35.56" x2="81.28" y2="35.56" width="0.1524" layer="91"/>
<label x="81.28" y="35.56" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="D2" gate="G$1" pin="-"/>
<wire x1="-34.29" y1="85.09" x2="-26.67" y2="85.09" width="0.1524" layer="91"/>
<label x="-8.89" y="85.09" size="1.27" layer="95" xref="yes"/>
<pinref part="CT1" gate="G$1" pin="+"/>
<wire x1="-26.67" y1="85.09" x2="-16.51" y2="85.09" width="0.1524" layer="91"/>
<junction x="-26.67" y="85.09"/>
<pinref part="C3" gate="G$1" pin="+"/>
<wire x1="-16.51" y1="85.09" x2="-8.89" y2="85.09" width="0.1524" layer="91"/>
<junction x="-16.51" y="85.09"/>
</segment>
<segment>
<pinref part="D1" gate="G$1" pin="VDD"/>
<wire x1="-3.81" y1="-52.07" x2="-5.08" y2="-52.07" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="-52.07" x2="-5.08" y2="-50.8" width="0.1524" layer="91"/>
<label x="-5.08" y="-50.8" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="VCC@2"/>
<pinref part="U2" gate="G$1" pin="VCC@1"/>
<wire x1="-30.48" y1="22.86" x2="-30.48" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="25.4" x2="-34.29" y2="25.4" width="0.1524" layer="91"/>
<junction x="-30.48" y="25.4"/>
<label x="-40.64" y="25.4" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="C31" gate="G$1" pin="1"/>
<wire x1="-34.29" y1="25.4" x2="-38.1" y2="25.4" width="0.1524" layer="91"/>
<junction x="-34.29" y="25.4"/>
<pinref part="C32" gate="G$1" pin="1"/>
<wire x1="-38.1" y1="25.4" x2="-40.64" y2="25.4" width="0.1524" layer="91"/>
<junction x="-38.1" y="25.4"/>
</segment>
<segment>
<wire x1="48.26" y1="22.86" x2="48.26" y2="25.4" width="0.1524" layer="91"/>
<label x="48.26" y="25.4" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="R1" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="AVCC"/>
<wire x1="-30.48" y1="48.26" x2="-31.75" y2="48.26" width="0.1524" layer="91"/>
<label x="-31.75" y="48.26" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="6"/>
<wire x1="78.74" y1="96.52" x2="81.28" y2="96.52" width="0.1524" layer="91"/>
<wire x1="81.28" y1="96.52" x2="81.28" y2="99.06" width="0.1524" layer="91"/>
<label x="81.28" y="99.06" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="-148.59" y1="27.94" x2="-148.59" y2="29.21" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="-148.59" y1="29.21" x2="-144.78" y2="29.21" width="0.1524" layer="91"/>
<wire x1="-144.78" y1="29.21" x2="-140.97" y2="29.21" width="0.1524" layer="91"/>
<wire x1="-140.97" y1="29.21" x2="-137.16" y2="29.21" width="0.1524" layer="91"/>
<wire x1="-137.16" y1="29.21" x2="-133.35" y2="29.21" width="0.1524" layer="91"/>
<wire x1="-133.35" y1="29.21" x2="-129.54" y2="29.21" width="0.1524" layer="91"/>
<wire x1="-129.54" y1="29.21" x2="-125.73" y2="29.21" width="0.1524" layer="91"/>
<wire x1="-125.73" y1="29.21" x2="-121.92" y2="29.21" width="0.1524" layer="91"/>
<wire x1="-121.92" y1="29.21" x2="-118.11" y2="29.21" width="0.1524" layer="91"/>
<wire x1="-118.11" y1="29.21" x2="-114.3" y2="29.21" width="0.1524" layer="91"/>
<wire x1="-114.3" y1="29.21" x2="-110.49" y2="29.21" width="0.1524" layer="91"/>
<wire x1="-110.49" y1="29.21" x2="-106.68" y2="29.21" width="0.1524" layer="91"/>
<wire x1="-106.68" y1="29.21" x2="-106.68" y2="27.94" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="-144.78" y1="27.94" x2="-144.78" y2="29.21" width="0.1524" layer="91"/>
<junction x="-144.78" y="29.21"/>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="-140.97" y1="27.94" x2="-140.97" y2="29.21" width="0.1524" layer="91"/>
<junction x="-140.97" y="29.21"/>
<pinref part="C15" gate="G$1" pin="2"/>
<wire x1="-137.16" y1="27.94" x2="-137.16" y2="29.21" width="0.1524" layer="91"/>
<junction x="-137.16" y="29.21"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="-133.35" y1="27.94" x2="-133.35" y2="29.21" width="0.1524" layer="91"/>
<junction x="-133.35" y="29.21"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="-129.54" y1="29.21" x2="-129.54" y2="27.94" width="0.1524" layer="91"/>
<junction x="-129.54" y="29.21"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="-125.73" y1="27.94" x2="-125.73" y2="29.21" width="0.1524" layer="91"/>
<junction x="-125.73" y="29.21"/>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="-121.92" y1="27.94" x2="-121.92" y2="29.21" width="0.1524" layer="91"/>
<junction x="-121.92" y="29.21"/>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="-118.11" y1="27.94" x2="-118.11" y2="29.21" width="0.1524" layer="91"/>
<junction x="-118.11" y="29.21"/>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="-114.3" y1="27.94" x2="-114.3" y2="29.21" width="0.1524" layer="91"/>
<junction x="-114.3" y="29.21"/>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="-110.49" y1="27.94" x2="-110.49" y2="29.21" width="0.1524" layer="91"/>
<junction x="-110.49" y="29.21"/>
<label x="-129.54" y="29.21" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="C26" gate="G$1" pin="2"/>
<wire x1="-148.59" y1="29.21" x2="-152.4" y2="29.21" width="0.1524" layer="91"/>
<wire x1="-152.4" y1="29.21" x2="-156.21" y2="29.21" width="0.1524" layer="91"/>
<wire x1="-156.21" y1="29.21" x2="-156.21" y2="27.94" width="0.1524" layer="91"/>
<junction x="-148.59" y="29.21"/>
<pinref part="C24" gate="G$1" pin="2"/>
<wire x1="-152.4" y1="27.94" x2="-152.4" y2="29.21" width="0.1524" layer="91"/>
<junction x="-152.4" y="29.21"/>
<pinref part="C27" gate="G$1" pin="2"/>
<wire x1="-106.68" y1="29.21" x2="-102.87" y2="29.21" width="0.1524" layer="91"/>
<wire x1="-102.87" y1="29.21" x2="-99.06" y2="29.21" width="0.1524" layer="91"/>
<wire x1="-99.06" y1="29.21" x2="-99.06" y2="27.94" width="0.1524" layer="91"/>
<junction x="-106.68" y="29.21"/>
<pinref part="C25" gate="G$1" pin="2"/>
<wire x1="-102.87" y1="27.94" x2="-102.87" y2="29.21" width="0.1524" layer="91"/>
<junction x="-102.87" y="29.21"/>
<pinref part="C23" gate="G$1" pin="2"/>
<wire x1="-62.23" y1="27.94" x2="-62.23" y2="29.21" width="0.1524" layer="91"/>
<wire x1="-62.23" y1="29.21" x2="-66.04" y2="29.21" width="0.1524" layer="91"/>
<junction x="-99.06" y="29.21"/>
<pinref part="C16" gate="G$1" pin="2"/>
<wire x1="-66.04" y1="29.21" x2="-69.85" y2="29.21" width="0.1524" layer="91"/>
<wire x1="-69.85" y1="29.21" x2="-74.93" y2="29.21" width="0.1524" layer="91"/>
<wire x1="-74.93" y1="29.21" x2="-80.01" y2="29.21" width="0.1524" layer="91"/>
<wire x1="-80.01" y1="29.21" x2="-85.09" y2="29.21" width="0.1524" layer="91"/>
<wire x1="-85.09" y1="29.21" x2="-90.17" y2="29.21" width="0.1524" layer="91"/>
<wire x1="-90.17" y1="29.21" x2="-95.25" y2="29.21" width="0.1524" layer="91"/>
<wire x1="-95.25" y1="29.21" x2="-99.06" y2="29.21" width="0.1524" layer="91"/>
<wire x1="-95.25" y1="27.94" x2="-95.25" y2="29.21" width="0.1524" layer="91"/>
<junction x="-95.25" y="29.21"/>
<pinref part="C17" gate="G$1" pin="2"/>
<wire x1="-90.17" y1="27.94" x2="-90.17" y2="29.21" width="0.1524" layer="91"/>
<junction x="-90.17" y="29.21"/>
<pinref part="C18" gate="G$1" pin="2"/>
<wire x1="-85.09" y1="27.94" x2="-85.09" y2="29.21" width="0.1524" layer="91"/>
<junction x="-85.09" y="29.21"/>
<pinref part="C19" gate="G$1" pin="2"/>
<wire x1="-80.01" y1="27.94" x2="-80.01" y2="29.21" width="0.1524" layer="91"/>
<junction x="-80.01" y="29.21"/>
<pinref part="C20" gate="G$1" pin="2"/>
<wire x1="-74.93" y1="27.94" x2="-74.93" y2="29.21" width="0.1524" layer="91"/>
<junction x="-74.93" y="29.21"/>
<pinref part="C21" gate="G$1" pin="2"/>
<wire x1="-69.85" y1="27.94" x2="-69.85" y2="29.21" width="0.1524" layer="91"/>
<junction x="-69.85" y="29.21"/>
<pinref part="C22" gate="G$1" pin="2"/>
<wire x1="-66.04" y1="27.94" x2="-66.04" y2="29.21" width="0.1524" layer="91"/>
<junction x="-66.04" y="29.21"/>
</segment>
<segment>
<pinref part="D3" gate="G$1" pin="VDD"/>
<wire x1="-38.1" y1="-52.07" x2="-39.37" y2="-52.07" width="0.1524" layer="91"/>
<wire x1="-39.37" y1="-52.07" x2="-39.37" y2="-50.8" width="0.1524" layer="91"/>
<label x="-39.37" y="-50.8" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="D4" gate="G$1" pin="VDD"/>
<wire x1="-72.39" y1="-52.07" x2="-73.66" y2="-52.07" width="0.1524" layer="91"/>
<wire x1="-73.66" y1="-52.07" x2="-73.66" y2="-50.8" width="0.1524" layer="91"/>
<label x="-73.66" y="-50.8" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="D5" gate="G$1" pin="VDD"/>
<wire x1="-105.41" y1="-52.07" x2="-106.68" y2="-52.07" width="0.1524" layer="91"/>
<wire x1="-106.68" y1="-52.07" x2="-106.68" y2="-50.8" width="0.1524" layer="91"/>
<label x="-106.68" y="-50.8" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="D6" gate="G$1" pin="VDD"/>
<wire x1="-138.43" y1="-52.07" x2="-139.7" y2="-52.07" width="0.1524" layer="91"/>
<wire x1="-139.7" y1="-52.07" x2="-139.7" y2="-50.8" width="0.1524" layer="91"/>
<label x="-139.7" y="-50.8" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="D7" gate="G$1" pin="VDD"/>
<wire x1="-171.45" y1="-52.07" x2="-172.72" y2="-52.07" width="0.1524" layer="91"/>
<wire x1="-172.72" y1="-52.07" x2="-172.72" y2="-50.8" width="0.1524" layer="91"/>
<label x="-172.72" y="-50.8" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="D8" gate="G$1" pin="VDD"/>
<wire x1="-204.47" y1="-52.07" x2="-205.74" y2="-52.07" width="0.1524" layer="91"/>
<wire x1="-205.74" y1="-52.07" x2="-205.74" y2="-50.8" width="0.1524" layer="91"/>
<label x="-205.74" y="-50.8" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="D9" gate="G$1" pin="VDD"/>
<wire x1="-238.76" y1="-52.07" x2="-240.03" y2="-52.07" width="0.1524" layer="91"/>
<wire x1="-240.03" y1="-52.07" x2="-240.03" y2="-50.8" width="0.1524" layer="91"/>
<label x="-240.03" y="-50.8" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="D10" gate="G$1" pin="VDD"/>
<wire x1="-3.81" y1="-77.47" x2="-5.08" y2="-77.47" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="-77.47" x2="-5.08" y2="-76.2" width="0.1524" layer="91"/>
<label x="-5.08" y="-76.2" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="D11" gate="G$1" pin="VDD"/>
<wire x1="-36.83" y1="-77.47" x2="-38.1" y2="-77.47" width="0.1524" layer="91"/>
<wire x1="-38.1" y1="-77.47" x2="-38.1" y2="-76.2" width="0.1524" layer="91"/>
<label x="-38.1" y="-76.2" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="D12" gate="G$1" pin="VDD"/>
<wire x1="-69.85" y1="-77.47" x2="-71.12" y2="-77.47" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="-77.47" x2="-71.12" y2="-76.2" width="0.1524" layer="91"/>
<label x="-71.12" y="-76.2" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="D13" gate="G$1" pin="VDD"/>
<wire x1="-102.87" y1="-77.47" x2="-104.14" y2="-77.47" width="0.1524" layer="91"/>
<wire x1="-104.14" y1="-77.47" x2="-104.14" y2="-76.2" width="0.1524" layer="91"/>
<label x="-104.14" y="-76.2" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="D14" gate="G$1" pin="VDD"/>
<wire x1="-138.43" y1="-78.74" x2="-139.7" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="-139.7" y1="-78.74" x2="-139.7" y2="-77.47" width="0.1524" layer="91"/>
<label x="-139.7" y="-77.47" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="D15" gate="G$1" pin="VDD"/>
<wire x1="-172.72" y1="-78.74" x2="-173.99" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="-173.99" y1="-78.74" x2="-173.99" y2="-77.47" width="0.1524" layer="91"/>
<label x="-173.99" y="-77.47" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="D16" gate="G$1" pin="VDD"/>
<wire x1="-207.01" y1="-78.74" x2="-208.28" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="-208.28" y1="-78.74" x2="-208.28" y2="-77.47" width="0.1524" layer="91"/>
<label x="-208.28" y="-77.47" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="D17" gate="G$1" pin="VDD"/>
<wire x1="-240.03" y1="-78.74" x2="-241.3" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="-241.3" y1="-78.74" x2="-241.3" y2="-77.47" width="0.1524" layer="91"/>
<label x="-241.3" y="-77.47" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="IN"/>
<wire x1="52.07" y1="-40.64" x2="50.8" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="50.8" y1="-40.64" x2="50.8" y2="-36.83" width="0.1524" layer="91"/>
<label x="50.8" y="-36.83" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="C30" gate="G$1" pin="1"/>
<junction x="52.07" y="-40.64"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="119.38" y1="-27.94" x2="119.38" y2="-26.67" width="0.1524" layer="91"/>
<label x="119.38" y="-26.67" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="163.83" y1="-30.48" x2="163.83" y2="-29.21" width="0.1524" layer="91"/>
<label x="163.83" y="-29.21" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="D18" gate="G$1" pin="VDD"/>
<wire x1="-69.85" y1="-26.67" x2="-71.12" y2="-26.67" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="-26.67" x2="-71.12" y2="-25.4" width="0.1524" layer="91"/>
<label x="-71.12" y="-25.4" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="D19" gate="G$1" pin="VDD"/>
<wire x1="-104.14" y1="-26.67" x2="-105.41" y2="-26.67" width="0.1524" layer="91"/>
<wire x1="-105.41" y1="-26.67" x2="-105.41" y2="-25.4" width="0.1524" layer="91"/>
<label x="-105.41" y="-25.4" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="D20" gate="G$1" pin="VDD"/>
<wire x1="-138.43" y1="-26.67" x2="-139.7" y2="-26.67" width="0.1524" layer="91"/>
<wire x1="-139.7" y1="-26.67" x2="-139.7" y2="-25.4" width="0.1524" layer="91"/>
<label x="-139.7" y="-25.4" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="D21" gate="G$1" pin="VDD"/>
<wire x1="-171.45" y1="-26.67" x2="-172.72" y2="-26.67" width="0.1524" layer="91"/>
<wire x1="-172.72" y1="-26.67" x2="-172.72" y2="-25.4" width="0.1524" layer="91"/>
<label x="-172.72" y="-25.4" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="D22" gate="G$1" pin="VDD"/>
<wire x1="-204.47" y1="-26.67" x2="-205.74" y2="-26.67" width="0.1524" layer="91"/>
<wire x1="-205.74" y1="-26.67" x2="-205.74" y2="-25.4" width="0.1524" layer="91"/>
<label x="-205.74" y="-25.4" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="D23" gate="G$1" pin="VDD"/>
<wire x1="-237.49" y1="-26.67" x2="-238.76" y2="-26.67" width="0.1524" layer="91"/>
<wire x1="-238.76" y1="-26.67" x2="-238.76" y2="-25.4" width="0.1524" layer="91"/>
<label x="-238.76" y="-25.4" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="D24" gate="G$1" pin="VDD"/>
<wire x1="-3.81" y1="-26.67" x2="-5.08" y2="-26.67" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="-26.67" x2="-5.08" y2="-25.4" width="0.1524" layer="91"/>
<label x="-5.08" y="-25.4" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="D25" gate="G$1" pin="VDD"/>
<wire x1="-36.83" y1="-26.67" x2="-38.1" y2="-26.67" width="0.1524" layer="91"/>
<wire x1="-38.1" y1="-26.67" x2="-38.1" y2="-25.4" width="0.1524" layer="91"/>
<label x="-38.1" y="-25.4" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="VUSB" class="0">
<segment>
<pinref part="USB1" gate="G$1" pin="VCC"/>
<wire x1="-96.52" y1="87.63" x2="-96.52" y2="90.17" width="0.1524" layer="91"/>
<label x="-96.52" y="90.17" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="F1" gate="G$1" pin="P$1"/>
<wire x1="-69.85" y1="85.09" x2="-74.93" y2="85.09" width="0.1524" layer="91"/>
<label x="-74.93" y="85.09" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="USB1" gate="G$1" pin="DM"/>
<wire x1="-96.52" y1="85.09" x2="-93.98" y2="85.09" width="0.1524" layer="91"/>
<wire x1="-93.98" y1="85.09" x2="-93.98" y2="82.55" width="0.1524" layer="91"/>
<pinref part="USB1" gate="G$1" pin="DP"/>
<wire x1="-93.98" y1="82.55" x2="-96.52" y2="82.55" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="F1" gate="G$1" pin="P$2"/>
<pinref part="D2" gate="G$1" pin="+"/>
<wire x1="-54.61" y1="85.09" x2="-41.91" y2="85.09" width="0.1524" layer="91"/>
</segment>
</net>
<net name="D2" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PD2(INT0/PCINT18)"/>
<wire x1="17.78" y1="5.08" x2="20.32" y2="5.08" width="0.1524" layer="91"/>
<label x="20.32" y="5.08" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="D24" gate="G$1" pin="DIN"/>
<wire x1="21.59" y1="-26.67" x2="19.05" y2="-26.67" width="0.1524" layer="91"/>
<label x="21.59" y="-26.67" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="D3" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PD3(INT1/OC2B/PCINT19)"/>
<wire x1="17.78" y1="2.54" x2="20.32" y2="2.54" width="0.1524" layer="91"/>
<label x="20.32" y="2.54" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="D4" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PD4(XCK/T0/CINT20)"/>
<wire x1="17.78" y1="0" x2="20.32" y2="0" width="0.1524" layer="91"/>
<label x="20.32" y="0" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="D5" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PD5(T1/OC0B/PCINT21)"/>
<wire x1="17.78" y1="-2.54" x2="20.32" y2="-2.54" width="0.1524" layer="91"/>
<label x="20.32" y="-2.54" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="D6" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PD6(AIN0/PC0A/PCINT22)"/>
<wire x1="17.78" y1="-5.08" x2="20.32" y2="-5.08" width="0.1524" layer="91"/>
<label x="20.32" y="-5.08" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="D7" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PD7(AIN1/PCINT23)"/>
<wire x1="20.32" y1="-7.62" x2="17.78" y2="-7.62" width="0.1524" layer="91"/>
<label x="20.32" y="-7.62" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="A0" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PC0(ADC0/PCINT8)"/>
<wire x1="17.78" y1="30.48" x2="20.32" y2="30.48" width="0.1524" layer="91"/>
<label x="20.32" y="30.48" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<pinref part="U4" gate="G$1" pin="P$2"/>
<wire x1="104.14" y1="11.43" x2="104.14" y2="10.16" width="0.1524" layer="91"/>
<wire x1="104.14" y1="10.16" x2="104.14" y2="8.89" width="0.1524" layer="91"/>
<wire x1="104.14" y1="10.16" x2="109.22" y2="10.16" width="0.1524" layer="91"/>
<junction x="104.14" y="10.16"/>
<label x="113.03" y="10.16" size="1.27" layer="95" xref="yes"/>
<pinref part="C33" gate="G$1" pin="2"/>
<wire x1="109.22" y1="10.16" x2="113.03" y2="10.16" width="0.1524" layer="91"/>
<wire x1="109.22" y1="6.35" x2="109.22" y2="10.16" width="0.1524" layer="91"/>
<junction x="109.22" y="10.16"/>
</segment>
</net>
<net name="A1" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PC1(ADC1PCINT9)"/>
<wire x1="17.78" y1="27.94" x2="20.32" y2="27.94" width="0.1524" layer="91"/>
<label x="20.32" y="27.94" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="A2" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PC2(ADC2PCINT10)"/>
<wire x1="17.78" y1="25.4" x2="20.32" y2="25.4" width="0.1524" layer="91"/>
<label x="20.32" y="25.4" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="A3" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PC3(ADC3/PCINT11)"/>
<wire x1="17.78" y1="22.86" x2="20.32" y2="22.86" width="0.1524" layer="91"/>
<label x="20.32" y="22.86" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PC4(ADC4/SDA/PCINT12)"/>
<wire x1="17.78" y1="20.32" x2="20.32" y2="20.32" width="0.1524" layer="91"/>
<label x="20.32" y="20.32" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PC5(ADC5/SCL/PCINT13)"/>
<wire x1="17.78" y1="17.78" x2="20.32" y2="17.78" width="0.1524" layer="91"/>
<label x="20.32" y="17.78" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="RESET" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PC6(PCINT14/!RESET)"/>
<wire x1="17.78" y1="15.24" x2="38.1" y2="15.24" width="0.1524" layer="91"/>
<wire x1="40.64" y1="10.16" x2="39.37" y2="10.16" width="0.1524" layer="91"/>
<wire x1="39.37" y1="10.16" x2="38.1" y2="10.16" width="0.1524" layer="91"/>
<wire x1="38.1" y1="10.16" x2="38.1" y2="15.24" width="0.1524" layer="91"/>
<wire x1="38.1" y1="15.24" x2="48.26" y2="15.24" width="0.1524" layer="91"/>
<wire x1="48.26" y1="15.24" x2="58.42" y2="15.24" width="0.1524" layer="91"/>
<wire x1="58.42" y1="15.24" x2="58.42" y2="10.16" width="0.1524" layer="91"/>
<pinref part="SW1" gate="G$1" pin="A1"/>
<wire x1="58.42" y1="10.16" x2="57.15" y2="10.16" width="0.1524" layer="91"/>
<pinref part="SW1" gate="G$1" pin="A0"/>
<junction x="39.37" y="10.16"/>
<junction x="38.1" y="15.24"/>
<label x="38.1" y="15.24" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="58.42" y1="15.24" x2="60.96" y2="15.24" width="0.1524" layer="91"/>
<junction x="58.42" y="15.24"/>
<pinref part="R1" gate="G$1" pin="1"/>
<junction x="48.26" y="15.24"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="2"/>
<wire x1="78.74" y1="86.36" x2="81.28" y2="86.36" width="0.1524" layer="91"/>
<label x="81.28" y="86.36" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="MOSI" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PB3(MOSI/OC2/PCINT3)"/>
<wire x1="17.78" y1="50.8" x2="20.32" y2="50.8" width="0.1524" layer="91"/>
<label x="20.32" y="50.8" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="78.74" y1="93.98" x2="81.28" y2="93.98" width="0.1524" layer="91"/>
<label x="81.28" y="93.98" size="1.27" layer="95" xref="yes"/>
<pinref part="J4" gate="G$1" pin="5"/>
</segment>
</net>
<net name="MISO" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PB4(MISO/PCINT4)"/>
<wire x1="17.78" y1="48.26" x2="20.32" y2="48.26" width="0.1524" layer="91"/>
<label x="20.32" y="48.26" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="78.74" y1="91.44" x2="81.28" y2="91.44" width="0.1524" layer="91"/>
<label x="81.28" y="91.44" size="1.27" layer="95" xref="yes"/>
<pinref part="J4" gate="G$1" pin="4"/>
</segment>
</net>
<net name="SCK" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PB5(SCK/PCINT5)"/>
<wire x1="17.78" y1="45.72" x2="20.32" y2="45.72" width="0.1524" layer="91"/>
<label x="20.32" y="45.72" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="78.74" y1="88.9" x2="81.28" y2="88.9" width="0.1524" layer="91"/>
<label x="81.28" y="88.9" size="1.27" layer="95" xref="yes"/>
<pinref part="J4" gate="G$1" pin="3"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PB7(XTAL2/TOSC2/PCINT7)"/>
<pinref part="Y1" gate="G$1" pin="1"/>
<wire x1="17.78" y1="35.56" x2="21.59" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PB6(XTAL1/TOSC1/PCINT6)"/>
<pinref part="Y1" gate="G$1" pin="3"/>
<wire x1="17.78" y1="40.64" x2="21.59" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="AREF"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="-30.48" y1="50.8" x2="-43.18" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="50.8" x2="-43.18" y2="49.53" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="DIN"/>
<pinref part="D1" gate="G$1" pin="DOUT"/>
<wire x1="-3.81" y1="-55.88" x2="-8.89" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="-8.89" y1="-55.88" x2="-8.89" y2="-52.07" width="0.1524" layer="91"/>
<wire x1="-8.89" y1="-52.07" x2="-15.24" y2="-52.07" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="D4" gate="G$1" pin="DIN"/>
<pinref part="D3" gate="G$1" pin="DOUT"/>
<wire x1="-38.1" y1="-55.88" x2="-43.18" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="-55.88" x2="-43.18" y2="-52.07" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="-52.07" x2="-49.53" y2="-52.07" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="D5" gate="G$1" pin="DIN"/>
<wire x1="-72.39" y1="-55.88" x2="-77.47" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="-77.47" y1="-55.88" x2="-77.47" y2="-52.07" width="0.1524" layer="91"/>
<wire x1="-77.47" y1="-52.07" x2="-82.55" y2="-52.07" width="0.1524" layer="91"/>
<pinref part="D4" gate="G$1" pin="DOUT"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="D6" gate="G$1" pin="DIN"/>
<wire x1="-105.41" y1="-55.88" x2="-110.49" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="-110.49" y1="-55.88" x2="-110.49" y2="-52.07" width="0.1524" layer="91"/>
<wire x1="-110.49" y1="-52.07" x2="-115.57" y2="-52.07" width="0.1524" layer="91"/>
<pinref part="D5" gate="G$1" pin="DOUT"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="D7" gate="G$1" pin="DIN"/>
<wire x1="-138.43" y1="-55.88" x2="-143.51" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="-143.51" y1="-55.88" x2="-143.51" y2="-52.07" width="0.1524" layer="91"/>
<wire x1="-143.51" y1="-52.07" x2="-148.59" y2="-52.07" width="0.1524" layer="91"/>
<pinref part="D6" gate="G$1" pin="DOUT"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="D8" gate="G$1" pin="DIN"/>
<pinref part="D7" gate="G$1" pin="DOUT"/>
<wire x1="-171.45" y1="-55.88" x2="-176.53" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="-176.53" y1="-55.88" x2="-176.53" y2="-52.07" width="0.1524" layer="91"/>
<wire x1="-176.53" y1="-52.07" x2="-181.61" y2="-52.07" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="D9" gate="G$1" pin="DIN"/>
<pinref part="D8" gate="G$1" pin="DOUT"/>
<wire x1="-204.47" y1="-55.88" x2="-209.55" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="-209.55" y1="-55.88" x2="-209.55" y2="-52.07" width="0.1524" layer="91"/>
<wire x1="-209.55" y1="-52.07" x2="-215.9" y2="-52.07" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="D10" gate="G$1" pin="DIN"/>
<pinref part="D9" gate="G$1" pin="DOUT"/>
<wire x1="-238.76" y1="-55.88" x2="-240.03" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="-240.03" y1="-55.88" x2="-240.03" y2="-66.04" width="0.1524" layer="91"/>
<wire x1="-240.03" y1="-66.04" x2="21.59" y2="-66.04" width="0.1524" layer="91"/>
<wire x1="21.59" y1="-66.04" x2="21.59" y2="-77.47" width="0.1524" layer="91"/>
<wire x1="21.59" y1="-77.47" x2="19.05" y2="-77.47" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="D11" gate="G$1" pin="DIN"/>
<wire x1="-3.81" y1="-81.28" x2="-8.89" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="-8.89" y1="-81.28" x2="-8.89" y2="-77.47" width="0.1524" layer="91"/>
<wire x1="-8.89" y1="-77.47" x2="-13.97" y2="-77.47" width="0.1524" layer="91"/>
<pinref part="D10" gate="G$1" pin="DOUT"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="D12" gate="G$1" pin="DIN"/>
<wire x1="-36.83" y1="-81.28" x2="-41.91" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="-41.91" y1="-81.28" x2="-41.91" y2="-77.47" width="0.1524" layer="91"/>
<wire x1="-41.91" y1="-77.47" x2="-46.99" y2="-77.47" width="0.1524" layer="91"/>
<pinref part="D11" gate="G$1" pin="DOUT"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="D13" gate="G$1" pin="DIN"/>
<wire x1="-69.85" y1="-81.28" x2="-74.93" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="-74.93" y1="-81.28" x2="-74.93" y2="-77.47" width="0.1524" layer="91"/>
<wire x1="-74.93" y1="-77.47" x2="-80.01" y2="-77.47" width="0.1524" layer="91"/>
<pinref part="D12" gate="G$1" pin="DOUT"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="D14" gate="G$1" pin="DIN"/>
<pinref part="D13" gate="G$1" pin="DOUT"/>
<wire x1="-102.87" y1="-81.28" x2="-109.22" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="-109.22" y1="-81.28" x2="-109.22" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="-109.22" y1="-78.74" x2="-115.57" y2="-78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="D15" gate="G$1" pin="DIN"/>
<pinref part="D14" gate="G$1" pin="DOUT"/>
<wire x1="-138.43" y1="-82.55" x2="-143.51" y2="-82.55" width="0.1524" layer="91"/>
<wire x1="-143.51" y1="-82.55" x2="-143.51" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="-143.51" y1="-78.74" x2="-149.86" y2="-78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="D16" gate="G$1" pin="DIN"/>
<pinref part="D15" gate="G$1" pin="DOUT"/>
<wire x1="-172.72" y1="-82.55" x2="-177.8" y2="-82.55" width="0.1524" layer="91"/>
<wire x1="-177.8" y1="-82.55" x2="-177.8" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="-177.8" y1="-78.74" x2="-184.15" y2="-78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="D17" gate="G$1" pin="DIN"/>
<wire x1="-207.01" y1="-82.55" x2="-212.09" y2="-82.55" width="0.1524" layer="91"/>
<wire x1="-212.09" y1="-82.55" x2="-212.09" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="-212.09" y1="-78.74" x2="-217.17" y2="-78.74" width="0.1524" layer="91"/>
<pinref part="D16" gate="G$1" pin="DOUT"/>
</segment>
</net>
<net name="3V3" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="OUT"/>
<wire x1="74.93" y1="-40.64" x2="76.2" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-40.64" x2="76.2" y2="-38.1" width="0.1524" layer="91"/>
<label x="76.2" y="-38.1" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="C28" gate="G$1" pin="+"/>
<junction x="76.2" y="-40.64"/>
<pinref part="C29" gate="G$1" pin="1"/>
<wire x1="76.2" y1="-40.64" x2="82.55" y2="-40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="3V3"/>
<wire x1="93.98" y1="-93.98" x2="92.71" y2="-93.98" width="0.1524" layer="91"/>
<label x="92.71" y="-93.98" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="113.03" y1="-31.75" x2="113.03" y2="-27.94" width="0.1524" layer="91"/>
<label x="113.03" y="-27.94" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="107.95" y1="-27.94" x2="113.03" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="Q1" gate="G$1" pin="G"/>
</segment>
<segment>
<wire x1="157.48" y1="-34.29" x2="157.48" y2="-30.48" width="0.1524" layer="91"/>
<label x="157.48" y="-30.48" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="Q2" gate="G$1" pin="G"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="152.4" y1="-30.48" x2="157.48" y2="-30.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="104.14" y1="19.05" x2="104.14" y2="21.59" width="0.1524" layer="91"/>
<label x="104.14" y="21.59" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="TXD" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="TX"/>
<wire x1="93.98" y1="-66.04" x2="91.44" y2="-66.04" width="0.1524" layer="91"/>
<label x="91.44" y="-66.04" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="Q1" gate="G$1" pin="S"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="107.95" y1="-35.56" x2="107.95" y2="-36.83" width="0.1524" layer="91"/>
<wire x1="107.95" y1="-36.83" x2="105.41" y2="-36.83" width="0.1524" layer="91"/>
<junction x="107.95" y="-36.83"/>
<label x="105.41" y="-36.83" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="RXD" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="RX"/>
<wire x1="93.98" y1="-68.58" x2="91.44" y2="-68.58" width="0.1524" layer="91"/>
<label x="91.44" y="-68.58" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<pinref part="Q2" gate="G$1" pin="S"/>
<wire x1="152.4" y1="-38.1" x2="152.4" y2="-39.37" width="0.1524" layer="91"/>
<junction x="152.4" y="-39.37"/>
<wire x1="152.4" y1="-39.37" x2="149.86" y2="-39.37" width="0.1524" layer="91"/>
<label x="149.86" y="-39.37" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="Q2" gate="G$1" pin="D"/>
<pinref part="R7" gate="G$1" pin="1"/>
<pinref part="SJ2" gate="1" pin="1"/>
<pinref part="SJ2" gate="1" pin="1"/>
<wire x1="165.1" y1="-39.37" x2="163.83" y2="-39.37" width="0.1524" layer="91"/>
<wire x1="163.83" y1="-39.37" x2="162.56" y2="-39.37" width="0.1524" layer="91"/>
<wire x1="163.83" y1="-38.1" x2="163.83" y2="-39.37" width="0.1524" layer="91"/>
<junction x="163.83" y="-39.37"/>
<junction x="165.1" y="-39.37"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="SJ1" gate="1" pin="1"/>
<pinref part="SJ1" gate="1" pin="1"/>
<pinref part="Q1" gate="G$1" pin="D"/>
<wire x1="118.11" y1="-36.83" x2="119.38" y2="-36.83" width="0.1524" layer="91"/>
<junction x="120.65" y="-36.83"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="119.38" y1="-36.83" x2="120.65" y2="-36.83" width="0.1524" layer="91"/>
<wire x1="119.38" y1="-35.56" x2="119.38" y2="-36.83" width="0.1524" layer="91"/>
<junction x="119.38" y="-36.83"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="D19" gate="G$1" pin="DIN"/>
<pinref part="D18" gate="G$1" pin="DOUT"/>
<wire x1="-69.85" y1="-30.48" x2="-74.93" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="-74.93" y1="-30.48" x2="-74.93" y2="-26.67" width="0.1524" layer="91"/>
<wire x1="-74.93" y1="-26.67" x2="-81.28" y2="-26.67" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="D20" gate="G$1" pin="DIN"/>
<pinref part="D19" gate="G$1" pin="DOUT"/>
<wire x1="-104.14" y1="-30.48" x2="-109.22" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="-109.22" y1="-30.48" x2="-109.22" y2="-26.67" width="0.1524" layer="91"/>
<wire x1="-109.22" y1="-26.67" x2="-115.57" y2="-26.67" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="D21" gate="G$1" pin="DIN"/>
<wire x1="-138.43" y1="-30.48" x2="-143.51" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="-143.51" y1="-30.48" x2="-143.51" y2="-26.67" width="0.1524" layer="91"/>
<wire x1="-143.51" y1="-26.67" x2="-148.59" y2="-26.67" width="0.1524" layer="91"/>
<pinref part="D20" gate="G$1" pin="DOUT"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="D22" gate="G$1" pin="DIN"/>
<wire x1="-171.45" y1="-30.48" x2="-176.53" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="-176.53" y1="-30.48" x2="-176.53" y2="-26.67" width="0.1524" layer="91"/>
<wire x1="-176.53" y1="-26.67" x2="-181.61" y2="-26.67" width="0.1524" layer="91"/>
<pinref part="D21" gate="G$1" pin="DOUT"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="D23" gate="G$1" pin="DIN"/>
<wire x1="-204.47" y1="-30.48" x2="-209.55" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="-209.55" y1="-30.48" x2="-209.55" y2="-26.67" width="0.1524" layer="91"/>
<wire x1="-209.55" y1="-26.67" x2="-214.63" y2="-26.67" width="0.1524" layer="91"/>
<pinref part="D22" gate="G$1" pin="DOUT"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="D23" gate="G$1" pin="DOUT"/>
<wire x1="-237.49" y1="-30.48" x2="-240.03" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="-240.03" y1="-30.48" x2="-240.03" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="-240.03" y1="-40.64" x2="21.59" y2="-40.64" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="DIN"/>
<wire x1="19.05" y1="-52.07" x2="21.59" y2="-52.07" width="0.1524" layer="91"/>
<wire x1="21.59" y1="-52.07" x2="21.59" y2="-40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="D25" gate="G$1" pin="DIN"/>
<wire x1="-3.81" y1="-30.48" x2="-8.89" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="-8.89" y1="-30.48" x2="-8.89" y2="-26.67" width="0.1524" layer="91"/>
<wire x1="-8.89" y1="-26.67" x2="-13.97" y2="-26.67" width="0.1524" layer="91"/>
<pinref part="D24" gate="G$1" pin="DOUT"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="D25" gate="G$1" pin="DOUT"/>
<wire x1="-36.83" y1="-30.48" x2="-41.91" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="-41.91" y1="-30.48" x2="-41.91" y2="-26.67" width="0.1524" layer="91"/>
<pinref part="D18" gate="G$1" pin="DIN"/>
<wire x1="-41.91" y1="-26.67" x2="-46.99" y2="-26.67" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
